function saveSetting(set) {
    layui.use(['form', 'layer', 'jquery'], function () {
        const form = layui.form;
        const $ = layui.jquery;
        form.on("submit("+ set + ")",function () {
            const params = $("#"+set+"-form").serialize();
            const url = '/admin/setting';
            $.ajax({
                url: url,
                type: 'POST',
                data: params,
                success: function (result) {
                    if (result.code === 0 && result) {
                        layer.msg("设置保存成功!", {icon: 1, time: 2000});
                        setTimeout(function () {
                            location.reload();
                        },2000)
                    } else {
                        layer.msg(result.msg || '设置保存失败', {icon: 2, time: 2000})
                    }
                }
            });
            return false;
        })
    })
}
