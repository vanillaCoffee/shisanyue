function subArticle(status) {
    layui.use(['form', 'layer', 'jquery', 'upload'], function () {
        const form = layui.form;
        const $ = layui.jquery;
        form.on("submit(" + status + ")", function (data) {
            // if (data.field.allowComment === "on") {
            //     $('#allow_comment').val(true);
            // } else if (data.field.allowComment !== "on") {
            //     $('#allow_comment').val(false);
            // }
            const content = $('#text').val();
            const pic = $('#credential_hide').val();
            $('#content-editor').val(content);
            $("#articleForm #status").val(status);
            const params = $("#articleForm").serialize();
            const url = $('#articleForm #cid').val() !== '' ? '/admin/article/modify' : '/admin/article/publish';
            if (pic === "") {
                layer.confirm('您确定要使用默认封面吗?', function (index) {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: params,
                        success: function (data) {
                            if (data.code === 0) {
                                layer.msg("文章发布成功!", {icon: 1, time: 2000});
                                setTimeout(function () {
                                    location.href = "/admin/article";
                                }, 2000)
                            } else {
                                layer.msg("文章发布失败!", {icon: 2, time: 2000})
                            }
                        }
                    });
                })
            } else {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: params,
                    success: function (data) {
                        if (data.code === 0) {
                            layer.msg("文章发布成功!", {icon: 1, time: 2000});
                            setTimeout(function () {
                                location.href = "/admin/article";
                            }, 2000)
                        } else {
                            layer.msg("文章发布失败!", {icon: 2, time: 2000})
                        }
                    }
                });
            }
            return false;
        });
    })
}


function delPost(cid) {
    layui.use(['layer', 'jquery', 'table'], function () {
        let $ = layui.jquery;
        layer.confirm('确定删除该文章吗?', function (index) {
            const load = layer.load(1, {shade: 0.3});
            $.post({
                    url: '/admin/article/delete',
                    data: {cid: cid},
                    success: function (data) {
                        layer.close(load);
                        if (data.code === 0) {
                            layer.msg("删除成功!", {icon: 1, time: 2000});
                        } else {
                            layer.msg(data.msg, {icon: 2, time: 2000});
                        }
                        setTimeout(function () {
                            location.reload();
                        }, 1500)
                    }
                }
            );
            layer.close(index);
        });
    })
}

// 置顶文章
function topArticle(cid, top) {
    // console.log(cid);
    // console.log(top);
    const title = top === 1 ? '是否置顶文章' : '是否取消置顶文章';
    layui.use(['layer', 'jquery', 'table'], function () {
        let $ = layui.jquery;
        layer.confirm(title, function (index) {
            const load = layer.load(1, {shade: 0.3});
            $.ajax({
                    url: '/admin/article/' + cid + '/' + top,
                    type: 'put',
                    success: function (data) {
                        layer.close(load);
                        if (data.code === 0) {
                            layer.msg(data.msg, {icon: 1, time: 2000});
                        } else {
                            layer.msg(data.msg, {icon: 2, time: 2000});
                        }
                        setTimeout(function () {
                            location.reload();
                        }, 1500)
                    }
                }
            );
            layer.close(index);
        });
    })
}

layui.use(['layer', 'upload'], function () {
    let $ = layui.jquery;
    let upload = layui.upload;
    upload.render({
        elem: '#upload_img'
        , url: '/admin/attach/upload'
        , size: 10240
        , exts: 'jpg|png|gif|bmp|jpeg|jfif'
        , auto: false //不自动上传
        , bindAction: '#upload' //上传绑定到隐藏按钮
        , choose: function (obj) { //预读本地文件
            obj.preview(function (index, file, result) {
                $('#fileName').val(file.name); //展示文件名
                $('#colla_img').find('img').remove();
                $('#colla_img').append('<a href="#" onclick="openImg();"><img id="showImg" src="' + result + '" width="370px"></a>');
            })
        }
        , done: function (res) {
            $('#credential_hide').val(res.date[0].src); //隐藏输入框赋值
            layer.msg("上传成功!", {icon: 1, time: 2000});
        }
        , error: function (index, upload) {
            layer.msg('上传失败！' + index, {icon: 2, time: 2000});
        }
    });
});

function openImg() {
    const idBar = '#showImg';
    const src = $(idBar)[0].src;
    const width = $(idBar).width();
    const height = $(idBar).height();
    const scaleWH = width / height;
    const bigH = 550;
    const bigW = scaleWH * bigH;
    if (bigW > 1000) {
        const bigW = 1000;
        const bigH = bigW / scaleWH;
    } // 放大预览图片
    parent.layer.open({
        type: 1,
        title: false,
        closeBtn: 1,
        shadeClose: true,
        area: [bigW + 'px', bigH + 'px'], //宽高
        content: '<img width="' + bigW + '" src="' + src + '" />'
    });
}
