function delPost(timeId) {
    layui.use(['layer', 'jquery', 'table'], function () {
        let $ = layui.jquery;
        layer.confirm('确定删除该日记吗?', function (index) {
            const load = layer.load(1, {shade: 0.3});
            $.ajax({
                    url: '/admin/timeline',
                    type: 'delete',
                    data: {timeId: timeId},
                    success: function (data) {
                        layer.close(load);
                        if (data.code === 0) {
                            layer.msg("删除成功!", {icon: 1, time: 2000});
                        } else {
                            layer.msg(data.msg, {icon: 2, time: 2000});
                        }
                        setTimeout(function () {
                            location.reload();
                        }, 1500)
                    }
                }
            );
            layer.close(index);
        });
    })
}



function subTimeline(status) {
    layui.use(['form', 'layer', 'jquery', 'upload'], function () {
        const form = layui.form;
        const $ = layui.jquery;
        form.on("submit(" + status + ")", function (data) {
            $("#diaryForm #status").val(status);
            const url = $('#diaryForm #tid').val() !== '' ? '/admin/timeline/edit' : '/admin/timeline/add';
            const params = $("#diaryForm").serialize();
            $.ajax({
                url: url,
                type: 'POST',
                data: params,
                success: function (data) {
                    if (data.code === 0) {
                        layer.msg(data.msg || "时光沙漏制作成功!", {icon: 1, time: 2000});
                        setTimeout(function () {
                            location.href = "/admin/timeline/list";
                        }, 2000)
                    } else {
                        layer.msg(data.msg || "时光沙漏制作失败!", {icon: 2, time: 2000})
                    }
                }
            });
        });
    })
}

