function addCategory(){
    layui.use(['layer', 'jquery', 'form'], function () {
        const $ = layui.jquery;
        const form = layui.form;
        form.on('submit(add)', function () {
            const cname = $('#addform #cname').val();
            if (cname !== '') {
                $.post({
                    url: '/admin/category/add',
                    data: {cname: cname},
                    success: function (result) {
                        $('#addform #cname').val('');
                        if (result && result.code === 0) {
                            layer.msg(result.msg || '新增分类成功', {icon: 1, time: 2000});

                        } else {
                            layer.msg(result.msg || '新增分类失败', {icon: 2, time: 2000});
                        }
                        setTimeout(function () {
                            location.reload();
                        },2000)
                    }
                });
            }
        })
    })
}

function editCategory() {
    $('.edit-category').click(function () {
        const mid = $(this).attr('mid');
        const cname = $(this).attr('cname');
        $('#updateform #mid').val(mid);
        $('#updateform #cname').val(cname);
    });
}


function updateCategory() {
    layui.use(['layer', 'jquery', 'form'], function () {
        const $ = layui.jquery;
        const form = layui.form;
        form.on('submit(update)', function () {
            const mid = $('#updateform #mid').val();
            const cname = $('#updateform #cname').val();
            if (mid && cname !== '') {
                $.post({
                    url: '/admin/category/update',
                    data: {mid:mid , cname: cname},
                    success: function (result) {
                        $('#updateform #mid').val('');
                        $('#updateform #cname').val('');
                        if (result && result.code === 0) {
                            layer.msg(result.msg || '更新分类成功' , {icon: 1, time: 2000});

                        } else {
                            layer.msg(result.msg || '更新分类失败', {icon: 2, time: 2000});
                        }
                        setTimeout(function () {
                            location.reload();
                        },2000)
                    }
                });
            }
        })
    })
}

function delPost() {
    $('.del-category').click(function () {
        const mid = $(this).attr('mid');
        layui.use(['layer', 'jquery', 'table'], function () {
            let $ = layui.jquery;
            layer.confirm('确定删除该项吗?', function (index) {
                const load = layer.load(1, {shade: 0.3});
                $.post({
                        url: '/admin/category/delete',
                        data: {mid: mid},
                        success: function (data) {
                            layer.close(load);
                            if (data.code === 0) {
                                layer.msg("删除成功!", {icon: 1, time: 2000});
                            } else {
                                layer.msg(data.msg, {icon: 2, time: 2000});
                            }
                            setTimeout(function () {
                                location.reload();
                            },1500)
                        }
                    }
                );
                layer.close(index);
            });
        })
    })
}