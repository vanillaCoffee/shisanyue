function delComment(coid) {
    layui.use(['layer','jquery','table'], function () {
        let $ = layui.jquery;
        layer.confirm('确定删除该评论吗?', function (index) {
            const load = layer.load(1, {shade: 0.3});
            $.post({
                    url: '/admin/comments/delete',
                    data: {coid: coid},
                    success: function (data) {
                        layer.close(load);
                        if (data.code === 0) {
                            layer.msg("删除成功!", {icon: 1});
                            setTimeout(function () {
                                location.reload();
                            },2000)
                        } else {
                            layer.msg(data.msg, {icon: 2});
                        }
                    }
                }
            );
            layer.close(index);
        });
    })
}

function updateStatus(coid) {
    layui.use(['layer','jquery','table'], function () {
        let $ = layui.jquery;
        layer.confirm('确定审核通过吗?', function (index) {
            const load = layer.load(1, {shade: 0.3});
            $.post({
                    url: '/admin/comments/status',
                    data: {coid: coid,status:'approved'},
                    success: function (data) {
                        layer.close(load);
                        if (data.code === 0) {
                            layer.msg("评论状态设置成功!", {icon: 1});
                            setTimeout(function () {
                                location.reload();
                            },2000)
                        } else {
                            layer.msg(data.msg, {icon: 2});
                        }
                    }
                }
            );
            layer.close(index);
        });
    })
}