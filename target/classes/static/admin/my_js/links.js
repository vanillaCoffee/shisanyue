function delLink(mid) {
    layui.use(['layer', 'jquery', 'table'], function () {
        let $ = layui.jquery;
        layer.confirm('确定删除该链接吗?', function (index) {
            const load = layer.load(1, {shade: 0.3});
            $.post({
                    url: '/admin/links/delete',
                    data: {mid: mid},
                    success: function (data) {
                        if (data.code === 0) {
                            layer.msg("删除成功!", {icon: 1});
                            setTimeout(function () {
                                location.reload();
                            },2000)
                        } else {
                            layer.msg(data.msg, {icon: 2});
                        }
                    }
                }
            );
            layer.close(index);
        });
    })
}

function saveLinks() {
    layui.use(['form','layer', 'jquery', 'table'], function () {
        const form = layui.form;
        let $ = layui.jquery;
        const params = $("#linkForm").serialize();
        form.on("submit(save)",function () {
            $.post({
                    url: '/admin/links/add',
                    data: params,
                    success: function (data) {
                        $('#linkForm input').val('');
                        $('#linkForm #sort').val('0');
                        if (data.code === 0) {
                            layer.msg("友链保存成功!", {icon: 1});
                            setTimeout(function () {
                                location.reload();
                            },2000)
                        } else {
                            layer.msg(data.msg || '友链保存失败', {icon: 2});
                        }
                    }
                }
            );
        });
    })
}

function editLink(obj) {
    const this_ = $(obj);

    const mid = this_.parents('tr').attr('mid');
    const title = this_.parents('tr').find('td:eq(0)').text();
    const url = this_.parents('tr').find('td:eq(1)').text();
    const logo = this_.parents('tr').find('td:eq(2)').text();
    const description = this_.parents('tr').find('td:eq(3)').text();
    const sort = this_.parents('tr').find('td:eq(4)').text();

    $('#linkForm #mid').val(mid);
    $('#linkForm #title').val(title);
    $('#linkForm #url').val(url);
    $('#linkForm #logo').val(logo);
    $('#linkForm #description').val(description);
    $('#linkForm #sort').val(sort);
}