function rePwd() {
    layui.use(['form', 'layer', 'jquery'], function () {
        const form = layui.form;
        const $ = layui.jquery;
        form.on("submit(repwd)",function () {
            const params = $("#rePwdForm").serialize();
            const url = '/admin/password';
            $.ajax({
                url: url,
                type: 'POST',
                data: params,
                success: function (result) {
                    if (result.code === 0 && result) {
                        layer.msg("密码修改成功!", {icon: 1, time: 2000});
                        setTimeout(function () {
                            location.href = "/toLogin";
                        },2000)
                    } else {
                        layer.msg(result.msg || '密码修改失败', {icon: 2, time: 2000})
                    }
                }
            });
            return false;
        })
    })
}

function setProInfo() {
    layui.use(['form', 'layer', 'jquery'], function () {
        const form = layui.form;
        const $ = layui.jquery;
        form.on("submit(setProInfo)",function () {
            const params = $("#user-form").serialize();
            const url = '/admin/profile';
            $.ajax({
                url: url,
                type: 'POST',
                data: params,
                success: function (result) {
                    if (result.code === 0 && result) {
                        layer.msg("个人信息修改成功!", {icon: 1, time: 2000});
                        setTimeout(function () {
                            location.reload();
                        },2000)
                    } else {
                        layer.msg(result.msg || '个人信息修改失败', {icon: 2, time: 2000})
                    }
                }
            });
            return false;
        })
    })
}

