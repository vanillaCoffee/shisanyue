﻿layui.use(['element', 'jquery', 'form', 'layedit', 'flow'], function () {
    const element = layui.element;
    const form = layui.form;
    const $ = layui.jquery;
    const layedit = layui.layedit;
    const flow = layui.flow;
    const indexs = [];
    const contents = [];
    const editIndex = layedit.build('commentEditor', {
        height: 180,
        tool: ['link', 'face']
    }); //建立编辑器
    form.render();
    // 评论和留言的编辑器的验证
    form.verify({
        content: function (value) {
            value = $.trim(layedit.getContent(editIndex));
            if (value === "") return "请输入内容";
            layedit.sync(editIndex);
        },

        replyContent: function (value) {
            if (value === "") return "请输入内容";
        }
    });


    //回复按钮点击事件
    $('#blog-comment').on('click', '.btn-reply', function () {
        const targetId = $(this).data('targetid')
            , targetName = $(this).data('targetname')
            , targetemail = $(this).data('targetemail')
            , $container = $(this).parent('p').parent().siblings('.replycontainer');
        if ($(this).text() === '回复') {
            $container.find('textarea').attr('placeholder', '回复【' + targetName + '】');
            $container.removeClass('layui-hide');
            $container.find('input[name="parent"]').val(targetId);
            $container.find('input[name="toemail"]').val(targetemail);
            $(this).parents('.blog-comment li').find('.btn-reply').text('回复');
            $(this).text('收起');
        } else {
            $container.addClass('layui-hide');
            $container.find('input[name="parent"]').val(0);
            $(this).text('回复');
        }
    });


});










 