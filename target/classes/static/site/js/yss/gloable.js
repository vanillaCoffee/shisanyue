﻿layui.use(['jquery', 'layer', 'util'], function () {
    const $ = layui.jquery,
        layer = layui.layer,
        util = layui.util;
    util.fixbar();
    //导航控制
    master.start($);
});
const slider = 0;
const pathname = window.location.pathname.replace('Read', 'Article');
const master = {};
master.start = function ($) {
    $('#nav li').hover(function () {
        $(this).addClass('current');
    }, function () {
        const href = $(this).find('a').attr("href");
        if (pathname.indexOf(href) === -1) {
            $(this).removeClass('current');
        }
    });
    selectNav();
    function selectNav() {
        const navobjs = $("#nav a");
        $.each(navobjs, function () {
            const href = $(this).attr("href");
            if (pathname.indexOf(href) !== -1) {
                $(this).parent().addClass('current');
            }
        });
    }
    $('.phone-menu').on('click', function () {
        $('#nav').toggle(500);
    });
    $(".blog-user img").hover(function () {
        const tips = layer.tips('点击退出', '.blog-user', {
            tips: [3, '#009688'],
        });
    }, function () {
        layer.closeAll('tips');
    })
};
