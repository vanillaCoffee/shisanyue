function overFn(obj) {//鼠标在上面
    $(obj).css("background", "#ff72ab");
}

function outFn(obj) {//鼠标离开
    $(obj).css("background", "white");
}

function clickFn(obj) {//鼠标点击
    $("#searchtxt").val($(obj).html());
    $("#child").css("display", "none");
}


//上面的是提示框的css代码，下面的是ajax获取代码
function searchWord(obj) {
    layui.use(['jquery'], function () {
        const $ = layui.jquery;
        //1、获得输入框的输入的内容
        var word = $(obj).val();
        var url = "/search/" + word;
        if (word === null || word === '') {
            var url = "/search/notfound";
            $(".search-result").css("display", "none");
        }
        //2、根据输入框的内容去数据库中模糊查询---List<Product>
        var content = "";

        $.ajax({
            url: url,
            type: 'get',
            dataType: "json",
            async: true,
            success: function (data) {
                if (data.code === 0) {
                    for (var i = 0; i < data.date.length; i++) {
                        var contentId = parseInt(data.date[i].contentId);
                        var title = data.date[i].title.toLowerCase();
                        var wordtemp = word.toLowerCase();
                        var titlefirst = data.date[i].title.toLowerCase().split(wordtemp)[0];
                        var titlesecond = data.date[i].title.toLowerCase().split(wordtemp)[1];
                        content += "<li class='child'><a href='/blog/article/" + contentId + "'" + ">"
                            + titlefirst +
                            "<b style='color:#6bc30d'>" + word + "</b>"
                            + titlesecond + "</a></li>";
                    }
                    $(".search-result").html(content);
                    $(".search-result").css("display", "block");
                    $(".child a").css("display", "block");
                } else {
                    $(".search-result").html("<li class='child'><a>" + '抱歉,没有找到您想要的文章' + "</a></li>");
                }
            }
        })
    })
}