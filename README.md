# shisanyue

#### 介绍
1. 十三月博客是一款由Java springboot驱动的单体开源博客项目。
2. 后台管理系统支持文章，作品，日记撰写，友链添加，仪表盘数据浏览，评论审核，文件上传，分类及标签的新增及编辑等功能，同时文章及作品支持网易云，虾米音乐外链为文章及作品提供背景音乐，采用markdown编辑及渲染，支持图片上传同时置为封面图片，文件存储采用七牛云oss，极大的提升了网站的加载速度。
3. 前台由燕十三博客模板为基础进行再开发及功能完善，集成动态搜索，动态文章目录，扁平化评论分级嵌套，同时支持评论留言用户的位置定位功能(可精确至市县)，采用博客首页采用Js实现loading动态加载等待功能，图片采用layui懒加载进行优化。
4. 本博客支持第三方QQ登录留言评论及qq邮箱回复等用户交互功能，同时网站引用了十三月音乐馆为游客提供音乐播放服务。

#### 软件架构
本项目为springboot开发的单体博客应用程序，博客前台采用燕十三博客模板，感谢大佬！
后台技术栈Sringboot + Springsecurity + themeleaf + mybatis + druid + mysql
- Idea = 2019.3
- Java = 1.8
- Springboot = 2.2.4.RELEASE
- Springsecurity = 2.0.0.M4
- MySQL = 8.0.19
- thymeleaf = thymeleaf-spring5


#### 安装配置

1.  拷贝blog.sql文件，导入mysql数据库
2.  配置application.yml 
3.  配置数据库用户名密码
4.  配置durid数据库连接池用户名密码
5.  配置七牛云cdn
6.  配置第三方QQ登录
7.  配置邮箱发送邮件配置


#### 使用说明

1.  完成以上安装配置后进入MyblogApplication.java，点击启动即可，本地启动需要修改application.yml中tomcat启动端口为80，并注释掉https的相关配置。
2.  部署linux服务器需先配置好java1.8，mysql8.0.19环境。
3.  使用maven的自动打包功能打好项目jar包，上传至服务器运行

Tips:本博客由Idea开发，JavaBean开发由lombok插件提供相关支持，若需要启动项目请提前安装好Idea lombok插件。

#### 参与贡献

1.  燕十三博客模板(www.yanshisan.cn)
2.  LayUI前端框架(www.layui.com)
3.  markdown在线编辑器editor.md(http://editor.md.ipandao.com)
4.  ip地址转换工具 Ip2Region(https://gitee.com/lionsoul/ip2region) 


#### 特别致谢

本博客前台使用燕十三大哥提供的模板，再次特别感谢！同时感谢Layui前端框架及其社区为本网站的顺利诞生提供的支持与帮助！！