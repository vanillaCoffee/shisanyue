package com.ssy.service.role;

import com.ssy.domain.RoleDomain;

import java.util.List;

public interface RoleService {
    int addRole(String roleName);

    int delRole(Integer roleId);

    int updateRole(RoleDomain roleDomain);

    List<RoleDomain> getRole();

    RoleDomain getRoleByRoleId(Integer roleId);
}
