package com.ssy.service.role.impl;

import com.ssy.dao.RoleDao;
import com.ssy.domain.RoleDomain;
import com.ssy.service.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleDao roleDao;

    @Override
    public int addRole(String roleName) {
        return roleDao.addRole(roleName);
    }

    @Override
    public int delRole(Integer roleId) {
        return roleDao.delRole(roleId);
    }

    @Override
    public int updateRole(RoleDomain roleDomain) {
        return roleDao.updateRole(roleDomain);
    }

    @Override
    public List<RoleDomain> getRole() {
        return roleDao.getRole();
    }

    @Override
    public RoleDomain getRoleByRoleId(Integer roleId) {
        return roleDao.getRoleByRoleId(roleId);
    }
}
