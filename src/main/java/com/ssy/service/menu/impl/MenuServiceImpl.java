package com.ssy.service.menu.impl;

import com.ssy.dao.MenuDao;
import com.ssy.domain.MenuDomain;
import com.ssy.domain.MetaDomain;
import com.ssy.service.menu.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {
    @Autowired
    MenuDao menuDao;

    @Override
    public int addMenu(MetaDomain metaDomain) {
        return menuDao.addMenu(metaDomain);
    }

    @Override
    public int delMenu(Integer metaId) {
        return menuDao.delMenu(metaId);
    }

    @Override
    public int updateMenu(MetaDomain metaDomain) {
        return menuDao.updateMenu(metaDomain);
    }

    @Override
    public List<MenuDomain> getMenu() {
        return menuDao.getMenu();
    }
}
