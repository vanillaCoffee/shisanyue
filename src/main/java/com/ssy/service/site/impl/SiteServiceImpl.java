package com.ssy.service.site.impl;

import com.github.pagehelper.PageHelper;
import com.ssy.dao.AttAchDao;
import com.ssy.dao.CommentDao;
import com.ssy.dao.ContentDao;
import com.ssy.dao.MetaDao;
import com.ssy.domain.CommentDomain;
import com.ssy.domain.ContentDomain;
import com.ssy.dto.CommentDto;
import com.ssy.dto.MetaDto;
import com.ssy.dto.StatisticsDto;
import com.ssy.dto.cond.CommentCond;
import com.ssy.dto.cond.ContentCond;
import com.ssy.constant.ExceptionEnum;
import com.ssy.constant.Types;
import com.ssy.exception.BusinessException;
import com.ssy.model.ArchiveVO;
import com.ssy.model.MessageVO;
import com.ssy.service.meta.MetaService;
import com.ssy.service.site.SiteService;
import com.ssy.utils.DateKit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class SiteServiceImpl implements SiteService {
    @Autowired
    private CommentDao commentDao;

    @Autowired
    private ContentDao contentDao;

    @Autowired
    private MetaDao metaDao;

    @Autowired
    private AttAchDao attAchDao;

    @Autowired
    private MetaService metaService;

    @Override
    public List<CommentDto> getComments(int limit) {
        if (limit < 0 || limit > 10) {
            limit = 10;
        }
        PageHelper.startPage(1, limit);
        return commentDao.getCommentsByCond(new CommentCond());
    }

    @Override
    public List<MessageVO> getRecentlyMessage(int limit) {
        if (limit < 0 || limit > 10) {
            limit = 10;
        }
        PageHelper.startPage(1, limit);
        return commentDao.getRecentlyMessage();
    }

    @Override
    public List<ContentDomain> getNewArticles(int limit) {
        if (limit < 0 || limit > 10)
            limit = 10;
        PageHelper.startPage(1, limit);
        return contentDao.getArticleByCond(new ContentCond());
    }

    @Override
    public CommentDomain getComment(Integer commentId) {
        if (null == commentId)
            throw new BusinessException(ExceptionEnum.PARAM_IS_EMPTY);
        return commentDao.getCommentsByCommentId(commentId);
    }

    @Override
    public StatisticsDto getStatistics() {
        Long articles = contentDao.getArticleCount();

        Long comments = commentDao.getCommentsCount();

        Long links = metaDao.getMetasCountByMetaType(Types.LINK.getType());

        Long attaches = attAchDao.getAttAchesCount();

        StatisticsDto rs = new StatisticsDto();
        rs.setArticles(articles);
        rs.setComments(comments);
        rs.setLinks(links);
        rs.setAttachs(attaches);
        return rs;
    }

    @Override
    public List<MetaDto> getMetaList(String type, String orderBy, int limit) {
        return metaService.getMetaListByMap(type,orderBy,limit);
    }

    @Override
    public List<ArchiveVO> getArchives(ContentCond contentCond) {
        List<ArchiveVO> archives = contentDao.getArchives(contentCond);
        parseArchives(archives, contentCond);
        return archives;
    }

    @Override
    public Long getCommentsCountByContentId(Integer contentId) {
        return commentDao.getCommentsCountByContentId(contentId);
    }

    private void parseArchives(List<ArchiveVO> archives, ContentCond contentCond) {
        if (null != archives) {
            archives.forEach(archive -> {
                String date = archive.getDate();
                Date sd = DateKit.dateFormat(date, "yyyy年MM月");
                int start = DateKit.getUnixTimeByDate(sd);
                int end = DateKit.getUnixTimeByDate(DateKit.dateAdd(DateKit.INTERVAL_MONTH, sd, 1)) - 1;
                ContentCond cond = new ContentCond();
                cond.setStartTime(start);
                cond.setEndTime(end);
                cond.setType(contentCond.getType());
                List<ContentDomain> contents = contentDao.getArticleByCond(cond);
                archive.setArticles(contents);
            });
        }
    }
}
