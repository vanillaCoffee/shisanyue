package com.ssy.service.site;

import com.ssy.domain.CommentDomain;
import com.ssy.domain.ContentDomain;
import com.ssy.dto.CommentDto;
import com.ssy.dto.MetaDto;
import com.ssy.dto.StatisticsDto;
import com.ssy.dto.cond.ContentCond;
import com.ssy.model.ArchiveVO;
import com.ssy.model.MessageVO;

import java.util.List;

public interface SiteService {
    /**
     * 获取评论列表
     */
    List<CommentDto> getComments(int limit);

    /**
     * 获取留言列表
     */
    List<MessageVO> getRecentlyMessage(int limit);

    /**
     * 获取最新文章
     */
    List<ContentDomain> getNewArticles(int limit);

    /**
     * 获取单条评论
     */
    CommentDomain getComment(Integer commentId);

    /**
     * 获取后台统计数据
     */
    StatisticsDto getStatistics();

    /**
     * 获取分类/标签列表
     */
    List<MetaDto> getMetaList(String type, String orderBy, int limit);

    /**
     * 获取归档列表
     */
    List<ArchiveVO> getArchives(ContentCond contentCond);


    /**
     * 获取累计评论数(通过文章号)
     */
    Long getCommentsCountByContentId(Integer contentId);
}
