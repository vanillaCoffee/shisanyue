package com.ssy.service.comment.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssy.dao.CommentDao;
import com.ssy.domain.CommentDomain;
import com.ssy.domain.ContentDomain;
import com.ssy.dto.CommentDto;
import com.ssy.dto.cond.CommentCond;
import com.ssy.constant.ExceptionEnum;
import com.ssy.exception.BusinessException;
import com.ssy.service.comment.CommentService;
import com.ssy.service.content.ContentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentDao commentDao;

    @Autowired
    private ContentService contentService;

    private static final Map<String, String> STATUS_MAP = new ConcurrentHashMap<>();

    /**
     * 评论状态：正常
     */
    private static final String STATUS_NORMAL = "approved";
    /**
     * 评论状态：不显示
     */
    private static final String STATUS_BLANK = "not_audit";

    static {
        STATUS_MAP.put("approved", STATUS_NORMAL);
        STATUS_MAP.put("not_audit", STATUS_BLANK);
    }

    @Override
    public int addComment(CommentDto comment) {
        ContentDomain article = contentService.getArticleByContentId(comment.getContentId());
        if (null == article)
            throw new BusinessException(ExceptionEnum.CONTENT_IS_NOT_EXIST);
//        Integer parentCommentId = comment.getParentComment().getCommentId();
//        if (parentCommentId != 0) {
//            comment.setParentComment(commentDao.getCommentsByCommentId(parentCommentId));
//        } else {
//            comment.setParentComment(null);
//        }
        comment.setStatus(STATUS_MAP.get(STATUS_BLANK));
        int res = commentDao.addComment(comment);
        Integer count = article.getCommentsNum();
        if (null == count) {
            count = 0;
        }
        count = count + 1;
        int res1 = contentService.updateArticleCommentCountByContentId(comment.getContentId(), count);
        return res & res1;
    }

    @Override
    public int addMessage(CommentDto message) {
//        Integer parentMessageId = message.getParentComment().getCommentId();
//        if (parentMessageId != 0) {
//            message.setParentComment(commentDao.getCommentsByCommentId(parentMessageId));
//        } else {
//            message.setParentComment(null);
//        }
        return commentDao.addComment(message);
    }

    @Override
    public int delComment(Integer commentId) {
        CommentCond commentCond = new CommentCond();
        commentCond.setParent(commentId);
        CommentDomain comment = commentDao.getCommentsByCommentId(commentId);
        List<CommentDto> childComments = commentDao.getCommentsByCond(commentCond);
        Integer count = 0;
        //删除子评论
        if (null != childComments && childComments.size() > 0) {
            for (CommentDomain childComment : childComments) {
                commentDao.delComment(childComment.getCommentId());
            }
        }
        //删除当前评论
        int res = commentDao.delComment(commentId);
        count++;

        //更新当前文章的评论数
        ContentDomain contentDomain = contentService.getArticleByContentId(comment.getContentId());
        if (null != contentDomain
                && null != contentDomain.getCommentsNum()
                && contentDomain.getCommentsNum() != 0) {
            contentDomain.setCommentsNum(contentDomain.getCommentsNum() - count);
            contentService.updateArticleByContentId(contentDomain);
        }
        return res;
    }

    @Override
    public int updateCommentStatus(Integer commentId, String status) {
        return commentDao.updateCommentStatus(commentId, status);
    }

    @Override
    public CommentDto getCommentsByCommentId(Integer commentId) {
        return commentDao.getCommentsByCommentId(commentId);
    }

    @Override
    public List<CommentDomain> getCommentsByContentId(Integer contentId) {
        return commentDao.getCommentsByContentId(contentId);
    }

    @Override
    public List<CommentDomain> getCommentsByContentIdAndApproved(Integer contentId) {
        return commentDao.getCommentsByContentIdAndApproved(contentId);
    }

    @Override
    public PageInfo<CommentDto> getCommentsByCond(CommentCond commentCond, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<CommentDto> comments = commentDao.getCommentsByCond(commentCond);
        return new PageInfo<>(comments);
    }

    @Override
    public List<CommentDto> getComments(CommentCond commentCond) {
        //查询出所有的父评论
        List<CommentDto> commentListMain = commentDao.getCommentsByCond(commentCond);

        //遍历所有父评论
        for (CommentDto comment : commentListMain) {
            CommentCond commentChild = new CommentCond();
            commentChild.setParent(comment.getCommentId());
            //查询出父评论的所有一级子评论
            List<CommentDto> commentListChildren = commentDao.getCommentsByCond(commentChild);
            //将一级子评论归附到它的父评论下
            comment.setChildren(commentListChildren);
        }
//        log.info("所有父评论" + commentListMain);
        return eachComment(commentListMain);
    }

    @Override
    public String getParentNickNameByParentId(Integer parentId) {
        return commentDao.getParentNickNameByParentId(parentId);
    }

    /**
     * 循环遍历复制每个顶级的评论节点
     */
    private List<CommentDto> eachComment(List<CommentDto> comments) {
        List<CommentDto> commentsView = new ArrayList<>();
        for (CommentDto comment : comments) {
            CommentDto c = new CommentDto();
            BeanUtils.copyProperties(comment, c);
            commentsView.add(c);
        }
//        log.info("复制过后的评论" + commentsView);
        combineChildren(commentsView);
        return commentsView;
    }

    private void combineChildren(List<CommentDto> comments) {
        //遍历父评论
        for (CommentDto comment : comments) {
            //获取二级评论(一级子评论)
            List<CommentDto> replys1 = comment.getChildren();
            for (CommentDto comment1 : replys1) {
                CommentCond commentChild = new CommentCond();
                commentChild.setParent(comment1.getCommentId());
                //查找出所有一级子评论的一级子评论
                List<CommentDto> commentListChildren = commentDao.getCommentsByCond(commentChild);
                comment1.setChildren(commentListChildren);
            }
            for (CommentDto reply1 : replys1) {
                //添加二级评论至全局容器中
                tempReplys.add(reply1);
                recursively(reply1);
            }
            //归并每个父评论下所有的子评论
            comment.setChildren(tempReplys);
            tempReplys = new ArrayList<>();
        }
//        log.info("所有评论"+comments);
    }

    private List<CommentDto> tempReplys = new ArrayList<>();


    private void recursively(CommentDto comment) {
        if (comment.getChildren().size() > 0) {
            //获取二级评论的子评论
            List<CommentDto> replys = comment.getChildren();
            for (CommentDto comment1 : replys) {
                CommentCond commentChild = new CommentCond();
                commentChild.setParent(comment1.getCommentId());
                List<CommentDto> commentListChildren = commentDao.getCommentsByCond(commentChild);
                comment1.setChildren(commentListChildren);
            }
//            log.info("二级评论的子评论"+replys);
            //遍历三级评论
            for (CommentDto reply : replys) {
                //添加三级评论至全局容器中
                tempReplys.add(reply);
                if (reply.getChildren().size() > 0) {

                    recursively(reply);
                }
            }
        }
    }
}
