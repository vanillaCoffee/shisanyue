package com.ssy.service.comment;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.CommentDomain;
import com.ssy.dto.CommentDto;
import com.ssy.dto.cond.CommentCond;

import java.util.List;

public interface CommentService {
    /**
     * 添加评论
     */
    int addComment(CommentDto comment);


    /**
     * 添加留言
     */
    int addMessage(CommentDto comment);

    /**
     * 删除评论
     */
    int delComment(Integer commentId);

    /**
     * 更新评论状态
     */
    int updateCommentStatus(Integer commentId, String status);

    /**
     * 根据主键查找单条评论
     */
    CommentDto getCommentsByCommentId(Integer commentId);

    /**
     * 根据文章编号查找单条评论
     */
    List<CommentDomain> getCommentsByContentId(Integer contentId);

    /**
     * 根据文章编号获取评论列表--只显示通过审核的评论-正常状态的
     */
    List<CommentDomain> getCommentsByContentIdAndApproved(Integer contentId);

    /**
     * 根据条件获取评论列表(分页)
     */
    PageInfo<CommentDto> getCommentsByCond(CommentCond commentCond, int pageNum, int pageSize);

    /**
     * 获取评论列表
     */
    List<CommentDto> getComments(CommentCond commentCond);

    /**
     * 通过父id获取父昵称
     */
    String getParentNickNameByParentId(Integer parentId);
}
