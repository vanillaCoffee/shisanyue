package com.ssy.service.attach.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssy.dao.AttAchDao;
import com.ssy.domain.AttAchDomain;
import com.ssy.dto.AttAchDto;
import com.ssy.model.AttAchesVO;
import com.ssy.service.attach.AttAchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttAchServiceImpl implements AttAchService {

    @Autowired
    private AttAchDao attAchDao;

    @Override
    public int addAttach(AttAchDomain attAchDomain) {
        return attAchDao.addAttAch(attAchDomain);
    }

    @Override
    public int batchAddAttAch(List<AttAchDomain> list) {
        return attAchDao.batchAddAttAch(list);
    }

    @Override
    public int delAttAch(Integer attachId) {
        return attAchDao.delAttAch(attachId);
    }

    @Override
    public int updateAttAch(AttAchDomain attAchDomain) {
        return attAchDao.updateAttAch(attAchDomain);
    }

    @Override
    public AttAchDto getAttAchByAttAchId(int attAchId) {
        return attAchDao.getAttAchByAttAchId(attAchId);
    }

    @Override
    public PageInfo<AttAchesVO> getAttAchesByFileType(String fileType,int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<AttAchesVO> attaches = attAchDao.getAttAchesByFileType(fileType);
        return new PageInfo<>(attaches);
    }

    @Override
    public PageInfo<AttAchDto> getAttAches(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<AttAchDto> attaches = attAchDao.getAttAches();
        return new PageInfo<>(attaches);
    }
}
