package com.ssy.service.attach;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.AttAchDomain;
import com.ssy.dto.AttAchDto;
import com.ssy.model.AttAchesVO;

import java.util.List;

public interface AttAchService {
    //添加单个附件
    int addAttach(AttAchDomain attAchDomain);

    //批量添加附件信息
    int batchAddAttAch(List<AttAchDomain> list);

    //根据主键编号删除附件信息
    int delAttAch(Integer attachId);

    //更新附件信息
    int updateAttAch(AttAchDomain attAchDomain);

    //根据主键获取附件信息
    AttAchDto getAttAchByAttAchId(int attAchId);

    //获取上传至七牛云的文章封面链接
    PageInfo<AttAchesVO> getAttAchesByFileType(String fileType,int pageNum, int pageSize);

    //获取所有的附件信息
    PageInfo<AttAchDto> getAttAches(int pageNum, int pageSize);
}
