package com.ssy.service.option.impl;

import com.ssy.dao.OptionDao;
import com.ssy.domain.OptionDomain;
import com.ssy.service.option.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OptionServiceImpl implements OptionService {
    @Autowired
    OptionDao optionDao;

    @Override
    public int delOptionByName(String name) {
        return optionDao.delOptionByName(name);
    }

    @Override
    public int updateOptionByName(OptionDomain option) {
        return optionDao.updateOptionByName(option);
    }

    @Override
    public OptionDomain getOptionByName(String name) {
        return optionDao.getOptionByName(name);
    }

    @Override
    public List<OptionDomain> getOptions() {
        return optionDao.getOptions();
    }
}
