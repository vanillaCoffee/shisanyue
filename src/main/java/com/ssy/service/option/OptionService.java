package com.ssy.service.option;

import com.ssy.domain.OptionDomain;

import java.util.List;

public interface OptionService {
    /**
     * 删除网站配置
     */
    int delOptionByName(String name);

    /**
     * 更新网站配置
     */
    int updateOptionByName(OptionDomain options);

    /***
     * 根据名称获取网站配置
     */
    OptionDomain getOptionByName(String name);

    /**
     * 获取全部网站配置
     */
    List<OptionDomain> getOptions();
}
