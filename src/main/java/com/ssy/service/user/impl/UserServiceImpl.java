package com.ssy.service.user.impl;

import com.ssy.dao.UserConnectionDao;
import com.ssy.dao.UserDao;
import com.ssy.domain.UserConnectionDomain;
import com.ssy.dto.UserDto;
import com.ssy.dto.cond.EmailCond;
import com.ssy.dto.cond.UserCond;
import com.ssy.dto.cond.UserConnectionCond;
import com.ssy.config.security.MyUserDetails;
import com.ssy.model.UserConnectionVO;
import com.ssy.model.UserVO;
import com.ssy.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private UserConnectionDao userConnectionDao;

    @Override
    public int updateUserInfoByUserCond(UserCond userCond) {
        return userDao.updateUserInfoByUserCond(userCond);
    }


    @Override
    public int updateUserAccountInfo(MyUserDetails userDetails) {
        return userDao.updateUserAccountInfo(userDetails);
    }


    @Override
    public MyUserDetails getUserInfoByUsername(String username) {
        return userDao.getUserInfoByUsername(username);
    }

    @Override
    public UserDto getUserInfoByUsernameAndSysUser(String username) {
        return userDao.getUserInfoByUsernameAndSysUser(username);
    }

    @Override
    public UserVO getUserVoByUsername(String username) {
        return userDao.getUserVoByUsername(username);
    }


    @Override
    public void addUserConnection(UserConnectionCond userConnection) {
        userConnectionDao.addUserConnection(userConnection);
    }

    @Override
    public void addEmailByProviderUserId(EmailCond emailCond) {
        userConnectionDao.addEmailByProviderUserId(emailCond);
    }

    @Override
    public ArrayList<String> getProviderUserIdList() {
        return userConnectionDao.getProviderUserIdList();
    }

    @Override
    public UserConnectionDomain getProviderByProviderUserId(String ProviderUserId) {
        return userConnectionDao.getProviderByProviderUserId(ProviderUserId);
    }

    @Override
    public List<UserConnectionVO> getUserHeadOrderByTime() {
        return userConnectionDao.getUserHeadOrderByTime();
    }

    @Override
    public UserConnectionVO getThirdUserInfoRecently() {
        return userConnectionDao.getThirdUserInfoRecently();
    }

    @Override
    public void delUserConnectionByProviderId(String providerId) {
        userConnectionDao.delUserConnectionByProviderId(providerId);
    }

    @Override
    public void setUserIdNull(String providerId) {
        userConnectionDao.setUserIdNull(providerId);
    }
}
