package com.ssy.service.user;

import com.ssy.domain.UserConnectionDomain;
import com.ssy.dto.UserDto;
import com.ssy.dto.cond.EmailCond;
import com.ssy.dto.cond.UserCond;
import com.ssy.dto.cond.UserConnectionCond;
import com.ssy.config.security.MyUserDetails;
import com.ssy.model.UserConnectionVO;
import com.ssy.model.UserVO;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface UserService {
    /**
     * 更新用户信息
     */
    int updateUserInfoByUserCond(UserCond userCond);


    /**
     * 更新用户账户信息
     */
    int updateUserAccountInfo(MyUserDetails userDetails);

    /**
     * 根据用户名获取用户信息(SpringSecurity userDetails)
     */
    MyUserDetails getUserInfoByUsername(String username);

    /**
     * 根据用户名获取用户信息(userDto)
     */
    UserDto getUserInfoByUsernameAndSysUser(@Param("username") String username);

    /**
     * 根据用户名获取用户信息(userVo)
     */
    UserVO getUserVoByUsername(@Param("username") String username);

    /**
     * 创建普通用户
     */
    void addUserConnection(UserConnectionCond userConnection);

    /**
     * 填补第三方用户邮箱
     */
    void addEmailByProviderUserId(EmailCond emailCond);

    /**
     * 获取ProviderUserId列表
     */
    ArrayList<String> getProviderUserIdList();

    /**
     * 通过QQ唯一标识获取QQ用户详情
     */
    UserConnectionDomain getProviderByProviderUserId(String ProviderUserId);

    /**
     * 根据登录时间获取用户头像，昵称列表
     */
    List<UserConnectionVO> getUserHeadOrderByTime();

    /**
     * 获取最近一次第三方登录的用户信息
     */
    UserConnectionVO getThirdUserInfoRecently();

    /**
     * 根据providerId删除第三方用户数据
     */
    void delUserConnectionByProviderId(String providerId);

    void setUserIdNull(String providerId);
}
