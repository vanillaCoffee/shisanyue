package com.ssy.service.userrole;

import com.ssy.domain.UserRoleDomain;

import java.util.List;

public interface UserRoleService {
    /**
     * 添加用户角色关联
     */
    int addUserRoleRelationShip(UserRoleDomain userRoleDomain);

    /**
     * 删除用户角色关联
     */
    int delUserRoleRelationShip(UserRoleDomain userRoleDomain);

    /**
     * 更新用户角色关联
     */
    int updateUserRoleRelationShip(UserRoleDomain userRoleDomain);

    /**
     * 获取用户角色列表
     */
    List<String> getRoleByRelationShipAndUsername(String username);
}
