package com.ssy.service.userrole.impl;

import com.ssy.dao.UserRoleDao;
import com.ssy.domain.UserRoleDomain;
import com.ssy.service.userrole.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserRoleServiceImpl implements UserRoleService {
    @Autowired
    UserRoleDao userRoleDao;

    @Override
    public int addUserRoleRelationShip(UserRoleDomain userRoleDomain) {
        return userRoleDao.addUserRoleRelationShip(userRoleDomain);
    }

    @Override
    public int delUserRoleRelationShip(UserRoleDomain userRoleDomain) {
        return userRoleDao.delUserRoleRelationShip(userRoleDomain);
    }

    @Override
    public int updateUserRoleRelationShip(UserRoleDomain userRoleDomain) {
        return userRoleDao.updateUserRoleRelationShip(userRoleDomain);
    }

    @Override
    public List<String> getRoleByRelationShipAndUsername(String username) {
        return userRoleDao.getRoleByRelationShipAndUsername(username);
    }
}
