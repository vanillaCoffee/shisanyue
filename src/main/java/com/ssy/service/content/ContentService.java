package com.ssy.service.content;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.ContentDomain;
import com.ssy.dto.cond.ContentCond;
import com.ssy.model.ContentVO;

import java.util.List;

public interface ContentService {
    /**
     * 新增文章
     */
    int addArticle(ContentDomain contentDomain);

    /**
     * 根据文章号删除文章
     */
    int delArticleByContentId(Integer contentId);

    /**
     * 更新文章
     */
    int updateArticleByContentId(ContentDomain contentDomain);

    /**
     * 根据文章号更新文章评论数
     */
    int updateArticleCommentCountByContentId(Integer contentId, Integer commentsNum);

    /**
     * 根据文章号获取文章信息
     */
    ContentDomain getArticleByContentId(Integer contentId);

    /**
     * 根据关键字获取文章标题列表
     */
    List<ContentVO> getSearchListByKeyWord(String keyWord);

    /**
     * 根据文章号获取文章信息
     */
    List<ContentDomain> getArticleListByCond(ContentCond contentCond);

    /**
     * 根据条件获取文章信息
     */
    PageInfo<ContentDomain> getArticleByCond(ContentCond contentCond, int pageNum, int pageSize);

    /**
     * 根据文章号获取前一篇文章和后一篇文章
     */
    List<ContentVO> getBeforeAndAfterArticle(Integer contentId);
}
