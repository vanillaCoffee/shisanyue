package com.ssy.service.content.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssy.dao.CommentDao;
import com.ssy.dao.ContentDao;
import com.ssy.dao.ContentMetaDao;
import com.ssy.domain.CommentDomain;
import com.ssy.domain.ContentDomain;
import com.ssy.domain.ContentMetaDomain;
import com.ssy.dto.cond.ContentCond;
import com.ssy.constant.Types;
import com.ssy.model.ContentVO;
import com.ssy.service.content.ContentService;
import com.ssy.service.meta.MetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContentServiceImpl implements ContentService {
    @Autowired
    ContentDao contentDao;

    @Autowired
    private MetaService metaService;

    @Autowired
    private ContentMetaDao contentMetaDao;

    @Autowired
    private CommentDao commentDao;

    @Override
    public int addArticle(ContentDomain contentDomain) {
        String tags = contentDomain.getTags();
        String categories = contentDomain.getCategories();
        int res = contentDao.addArticle(contentDomain);
        int cid = contentDomain.getContentId();
        metaService.addMeta(cid, tags, Types.TAG.getType());
        metaService.addMeta(cid, categories, Types.CATEGORY.getType());
        return res;
    }

    @Override
    public int delArticleByContentId(Integer contentId) {
        int res = contentDao.delArticleByContentId(contentId);
        //同时也要删除该文章下的所有评论
        List<CommentDomain> comments = commentDao.getCommentsByContentId(contentId);
        if (null != comments && comments.size() > 0) {
            comments.forEach(comment -> {
                commentDao.delComment(comment.getCommentId());
            });
        }
        //删除标签和分类关联
        List<ContentMetaDomain> relationShips = contentMetaDao.getContentMetaRelationShipByContentId(contentId);
        if (null != relationShips && relationShips.size() > 0) {
            contentMetaDao.delContentMetaRelationShipByContentId(contentId);
        }
        return res;
    }

    @Override
    public int updateArticleByContentId(ContentDomain contentDomain) {
        //标签和分类
        String tags = contentDomain.getTags();
        String categories = contentDomain.getCategories();

        int res = contentDao.updateArticleByContentId(contentDomain);
        int cid = contentDomain.getContentId();
        contentMetaDao.delContentMetaRelationShipByContentId(cid);
        metaService.addMeta(cid, tags, Types.TAG.getType());
        metaService.addMeta(cid, categories, Types.CATEGORY.getType());

        return res;
    }

    @Override
    public int updateArticleCommentCountByContentId(Integer contentId, Integer commentsNum) {
        return contentDao.updateArticleCommentCountByContentId(contentId, commentsNum);
    }

    @Override
    public ContentDomain getArticleByContentId(Integer contentId) {
        return contentDao.getArticleByContentId(contentId);
    }

    @Override
    public List<ContentVO> getSearchListByKeyWord(String keyWord) {
        return contentDao.getSearchListByKeyWord(keyWord);
    }

    @Override
    public List<ContentDomain> getArticleListByCond(ContentCond contentCond) {
        return contentDao.getArticleByCond(contentCond);
    }

    @Override
    public PageInfo<ContentDomain> getArticleByCond(ContentCond contentCond, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<ContentDomain> contentDomains = contentDao.getArticleByCond(contentCond);
        return new PageInfo<>(contentDomains);
    }

    @Override
    public List<ContentVO> getBeforeAndAfterArticle(Integer contentId) {
        List<ContentVO> beforeAndAfterArticle = contentDao.getBeforeAndAfterArticle(contentId);
        if (beforeAndAfterArticle.size() > 1) {
            return beforeAndAfterArticle;
        } else {
            if (contentId > beforeAndAfterArticle.get(0).getContentId()) {
                beforeAndAfterArticle.add(new ContentVO(contentId,"没有了......"));
                return beforeAndAfterArticle;
            } else {
                beforeAndAfterArticle.add(0,new ContentVO(contentId,"没有了......"));
                return beforeAndAfterArticle;
            }
        }
    }
}
