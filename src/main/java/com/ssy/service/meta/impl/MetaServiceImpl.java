package com.ssy.service.meta.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssy.dao.ContentMetaDao;
import com.ssy.dao.MetaDao;
import com.ssy.domain.ContentDomain;
import com.ssy.domain.ContentMetaDomain;
import com.ssy.domain.MetaDomain;
import com.ssy.dto.MetaDto;
import com.ssy.dto.cond.MetaCond;
import com.ssy.constant.Types;
import com.ssy.constant.WebConst;
import com.ssy.exception.BusinessException;
import com.ssy.constant.ExceptionEnum;
import com.ssy.service.content.ContentService;
import com.ssy.service.meta.MetaService;
import com.ssy.utils.StringUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class MetaServiceImpl implements MetaService {
    @Autowired
    MetaDao metaDao;

    @Autowired
    ContentMetaDao contentMetaDao;

    @Autowired
    ContentService contentService;

    @Override
    public int addMeta(String type, String name) {
        if (StringUtils.isNotBlank(type) && StringUtils.isNotBlank(name)) {
            MetaCond metaCond = new MetaCond();
            metaCond.setMetaName(name);
            metaCond.setMetaType(type);
            //先根据条件查询是否已有该项目，若没有该项目则更新/新增分类
            List<MetaDomain> metas = metaDao.getMetasByCond(metaCond);
            if (null == metas || metas.size() == 0) {
                MetaDomain metaDomain = new MetaDomain();
                metaDomain.setMetaName(name);
                metaDomain.setMetaType(type);
                return metaDao.addMeta(metaDomain);
            } else {
                throw new BusinessException(ExceptionEnum.META_IS_EXIST);
            }
        } else {
            throw new BusinessException(ExceptionEnum.PARAM_IS_EMPTY);
        }
    }

    @Override
    public int addMeta(MetaDomain metaDomain) {
        return metaDao.addMeta(metaDomain);
    }

    @Override
    public int addMeta(Integer cid, String names, String type) {
        if (null == cid)
            throw new BusinessException(ExceptionEnum.PARAM_IS_EMPTY);

        if (StringUtils.isNotBlank((names)) && StringUtils.isNotBlank(type)) {
            String[] nameArr = StringUtils.split(names, ",");
            for (String name : nameArr) {
                this.addOrUpdate(cid, name, type);
            }
        }
        return 1;
    }

    @Override
    public int delMetaByMetaId(Integer metaId) {
        if (null == metaId)
            throw new BusinessException(ExceptionEnum.PARAM_IS_EMPTY);
        MetaDomain metaDomain = metaDao.getMetaByMetaId(metaId);
        if (null != metaDomain) {
            String MetaType = metaDomain.getMetaType();
            String MetaName = metaDomain.getMetaName();
            //先从sys_metas表中删除掉分类
            int res= metaDao.delMetaByMetaId(metaId);
            //再在sys_content_meta表中查出该分类与其关联的所有文章的关系
            List<ContentMetaDomain> relationShips = contentMetaDao.getContentMetaRelationShipByMetaId(metaId);
            if (null != relationShips && relationShips.size() > 0){
                //循环遍历刚刚查找出的关系列表
                for (ContentMetaDomain relationShip: relationShips){
                    ContentDomain article = contentService.getArticleByContentId(relationShip.getContentId());
                    if (null != article){
                        ContentDomain temp = new ContentDomain();
                        temp.setContentId(relationShip.getContentId());
                        if (MetaType.equalsIgnoreCase(Types.CATEGORY.getType())) {
                            temp.setCategories(StringUtil.updateMeta(MetaName,article.getCategories()));
                        }
                        if (MetaType.equalsIgnoreCase(Types.TAG.getType())) {
                            temp.setTags(StringUtil.updateMeta(MetaName,article.getTags()));
                        }
                        //在sys_contents表中更新categories或tags字段来实现文章或作品分类或标签的删除
                        contentService.updateArticleByContentId(temp);
                    }else {
                        return 1;
                    }
                }
            }
            //最后在sys_content_meta表中删除掉该分类与其关联的所有文章的关系
            return contentMetaDao.delContentMetaRelationShipByMetaId(metaId) & res;
        }else {
            throw new BusinessException(ExceptionEnum.PARAM_IS_EMPTY);
        }
    }

    @Override
    public int updateMeta(MetaDomain metaDomain) {
        return metaDao.updateMeta(metaDomain);
    }

    @Override
    public int updateMetaName(MetaCond metaCond) {
        return metaDao.updateMetaName(metaCond);
    }

    @Override
    public int addOrUpdate(Integer cid, String name, String type) {
        //条件查询是否已有匹配的的项目
        MetaCond metaCond = new MetaCond();
        metaCond.setMetaName(name);
        metaCond.setMetaType(type);
        List<MetaDomain> metas = this.getMetasByCond(metaCond);
        int mid;
        MetaDomain metaDomain;
        //如果存在这样的项目则获取到项目编号，如果不存在则新增分类/标签。
        if (metas.size() == 1) {
            MetaDomain meta = metas.get(0);
            mid = meta.getMetaId();
        } else if (metas.size() > 1) {
            throw new BusinessException(ExceptionEnum.NOT_ONE_RESULT);
        } else {
            metaDomain = new MetaDomain();
            metaDomain.setSlug(name);
            metaDomain.setMetaName(name);
            metaDomain.setMetaType(type);
            this.addMeta(metaDomain);
            mid = metaDomain.getMetaId();
        }
        // 如果该项目存在则查询是否已和该文章建立关联，如果为建立关联则建立关联
        if (mid != 0) {
            Long count = contentMetaDao.getCountByContentIdAndMetaId(cid, mid);
            if (count == 0) {
                ContentMetaDomain relationShip = new ContentMetaDomain();
                relationShip.setContentId(cid);
                relationShip.setMetaId(mid);
                contentMetaDao.addContentMetaRelationShip(relationShip);
            }
        }
        return 1;
    }

    @Override
    public MetaDomain getMetaByMetaId(Integer metaId) {
        if (null == metaId)
            throw new BusinessException(ExceptionEnum.PARAM_IS_EMPTY);
        return metaDao.getMetaByMetaId(metaId);
    }

    @Override
    public List<MetaDomain> getMetasByCond(MetaCond metaCond) {
        return metaDao.getMetasByCond(metaCond);
    }

    @Override
    public PageInfo<MetaDomain> getMetasByCondWithPage(MetaCond metaCond, int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<MetaDomain> metas = metaDao.getMetasByCondWithPage(metaCond);
        return new PageInfo<>(metas);
    }

    @Override
    public List<MetaDto> getMetaListByMap(String type, String orderBy, int limit) {
        if (StringUtils.isNotBlank(type)){
            if (StringUtils.isBlank(orderBy)) {
                orderBy = "count desc,a.meta_id desc";
            }
            if (limit < 1 || limit > WebConst.MAX_POSTS) {
                limit = 10;
            }
            Map<String, Object> paraMap = new HashMap<>();
            paraMap.put("metaType", type);
            paraMap.put("order", orderBy);
            paraMap.put("limit", limit);
            return metaDao.getMetaList(paraMap);
        }
        return null;
    }
}
