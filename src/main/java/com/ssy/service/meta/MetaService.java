package com.ssy.service.meta;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.MetaDomain;
import com.ssy.dto.MetaDto;
import com.ssy.dto.cond.MetaCond;

import java.util.List;

public interface MetaService {

    /**
     * 添加分类
     */
    int addMeta(String type, String name);


    /**
     * 添加分类
     */
    int addMeta(MetaDomain metaDomain);

    /**
     * 添加分类
     */
    int addMeta(Integer cid, String names, String type);

    /**
     * 删除分类
     */
    int delMetaByMetaId(Integer metaId);


    /**
     * 更新分类
     */
    int updateMeta(MetaDomain metaDomain);

    /**
     * 更新分类名
     */
    int updateMetaName(MetaCond metaCond);

    /**
     * (新增/更新)文章同时(新增/更新)文章与分类/标签之间的关系
     */
    int addOrUpdate(Integer cid, String name, String type);

    /**
     * 根据编号获取项目
     */
    MetaDomain getMetaByMetaId(Integer metaDomain);


    /**
     * 根据条件查询获取分类列表
     */
    List<MetaDomain> getMetasByCond(MetaCond metaCond);

    /**
     * 根据条件查询获取分类列表
     */
    PageInfo<MetaDomain> getMetasByCondWithPage(MetaCond metaCond,int pageNum, int pageSize);

    /**
     * 根据类型查询项目列表，带项目下面的文章数
     */
    List<MetaDto> getMetaListByMap(String type, String orderBy, int limit);
}
