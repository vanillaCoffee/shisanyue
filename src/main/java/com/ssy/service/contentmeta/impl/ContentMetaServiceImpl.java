package com.ssy.service.contentmeta.impl;

import com.ssy.dao.ContentMetaDao;
import com.ssy.domain.ContentMetaDomain;
import com.ssy.service.contentmeta.ContentMetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContentMetaServiceImpl implements ContentMetaService {
    @Autowired
    ContentMetaDao contentMetaDao;

    @Override
    public int addContentMetaRelationShip(ContentMetaDomain contentMetaDomain) {
        return contentMetaDao.addContentMetaRelationShip(contentMetaDomain);
    }

    @Override
    public int delContentMetaRelationShip(ContentMetaDomain contentMetaDomain) {
        return contentMetaDao.delContentMetaRelationShip(contentMetaDomain);
    }

    @Override
    public int delContentMetaRelationShipByContentId(Integer contentId) {
        return contentMetaDao.delContentMetaRelationShipByContentId(contentId);
    }

    @Override
    public int delContentMetaRelationShipByMetaId(Integer metaId) {
        return contentMetaDao.delContentMetaRelationShipByMetaId(metaId);
    }

    @Override
    public int updateContentMetaRelationShip(ContentMetaDomain contentMetaDomain) {
        return contentMetaDao.updateContentMetaRelationShip(contentMetaDomain);
    }

    @Override
    public List<ContentMetaDomain> getContentMetaRelationShipByContentId(Integer contentId) {
        return contentMetaDao.getContentMetaRelationShipByContentId(contentId);
    }

    @Override
    public List<ContentMetaDomain> getContentMetaRelationShipByMetaId(Integer metaId) {
        return contentMetaDao.getContentMetaRelationShipByMetaId(metaId);
    }
}
