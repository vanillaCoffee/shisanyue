package com.ssy.service.log;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.LogDomain;

public interface LogService {
    /**
     * 添加日志
     */
    int addLog(String action, String data, String ip, Integer authorId);

    /**
     * 删除日志
     */
    int delLogById(Integer id);

    /**
     * 获取日志
     */
    PageInfo<LogDomain> getLogs(int pageNum, int pageSize);
}
