package com.ssy.service.log.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssy.dao.LogDao;
import com.ssy.domain.LogDomain;
import com.ssy.dto.cond.LogCond;
import com.ssy.constant.ExceptionEnum;
import com.ssy.exception.BusinessException;
import com.ssy.service.log.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogDao logDao;

    @Override
    public int addLog(String action, String data, String ip, Integer authorId) {
        LogCond logCond = new LogCond();
        logCond.setAuthorId(authorId);
        logCond.setIp(ip);
        logCond.setData(data);
        logCond.setAction(action);
        return logDao.addLog(logCond);
    }

    @Override
    public int delLogById(Integer id) {
        if (null == id)
            throw new BusinessException(ExceptionEnum.PARAM_IS_EMPTY);

        return delLogById(id);
    }

    @Override
    public PageInfo<LogDomain> getLogs(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<LogDomain> logs = logDao.getLogs();
        return new PageInfo<>(logs);
    }
}
