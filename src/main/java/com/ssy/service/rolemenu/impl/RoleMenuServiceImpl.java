package com.ssy.service.rolemenu.impl;


import com.ssy.dao.RoleMenuDao;
import com.ssy.domain.RoleMenuDomain;
import com.ssy.service.rolemenu.RoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleMenuServiceImpl implements RoleMenuService {
    @Autowired
    RoleMenuDao roleMenuDao;

    @Override
    public int addRoleMenuRelationShip(RoleMenuDomain roleMenuDomain) {
        return roleMenuDao.addRoleMenuRelationShip(roleMenuDomain);
    }

    @Override
    public int delRoleMenuRelationShip(RoleMenuDomain roleMenuDomain) {
        return roleMenuDao.delRoleMenuRelationShip(roleMenuDomain);
    }

    @Override
    public int updateRoleMenuRelationShip(RoleMenuDomain roleMenuDomain) {
        return roleMenuDao.updateRoleMenuRelationShip(roleMenuDomain);
    }

    @Override
    public List<String> getMenuByRelationShipAndRoleList(List<String> roleList) {
        return roleMenuDao.getMenuByRelationShipAndRoleList(roleList);
    }
}
