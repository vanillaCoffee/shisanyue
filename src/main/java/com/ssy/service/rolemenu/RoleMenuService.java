package com.ssy.service.rolemenu;

import com.ssy.domain.RoleMenuDomain;

import java.util.List;

public interface RoleMenuService {
    /**
     * 添加角色权限关联
     */
    int addRoleMenuRelationShip(RoleMenuDomain roleMenuDomain);

    /**
     * 删除角色权限关联
     */
    int delRoleMenuRelationShip(RoleMenuDomain roleMenuDomain);

    /**
     * 更新角色权限关联
     */
    int updateRoleMenuRelationShip(RoleMenuDomain roleMenuDomain);

    /**
     * 获取角色权限关联列表
     */
    List<String> getMenuByRelationShipAndRoleList(List<String> RoleList);
}
