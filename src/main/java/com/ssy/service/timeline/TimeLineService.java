package com.ssy.service.timeline;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.TimeDomain;


import java.util.List;

public interface TimeLineService {
    int addTimeLine(TimeDomain timeDomain);

    int delTimeLine(Integer timeId);

    int updateTimeLine(TimeDomain timeDomain);

    TimeDomain getTimeLineByTimeId(Integer timeId);

    List<TimeDomain> getTimeLineList();

    PageInfo<TimeDomain> getTimeLine(int pageNum, int pageSize);
}
