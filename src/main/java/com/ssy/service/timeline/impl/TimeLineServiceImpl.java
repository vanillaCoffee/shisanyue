package com.ssy.service.timeline.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.ssy.dao.TimeDao;
import com.ssy.domain.TimeDomain;
import com.ssy.service.timeline.TimeLineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TimeLineServiceImpl implements TimeLineService {

    @Autowired
    private TimeDao timeDao;

    @Override
    public int addTimeLine(TimeDomain timeDomain) {
        return timeDao.addTimeLine(timeDomain);
    }

    @Override
    public int delTimeLine(Integer timeId) {
        return timeDao.delTimeLine(timeId);
    }

    @Override
    public int updateTimeLine(TimeDomain timeDomain) {
        return timeDao.updateTimeLine(timeDomain);
    }

    @Override
    public TimeDomain getTimeLineByTimeId(Integer timeId) {
        return timeDao.getTimeLineByTimeId(timeId);
    }

    @Override
    public List<TimeDomain> getTimeLineList() {
        return timeDao.getTimeLine();
    }

    @Override
    public PageInfo<TimeDomain> getTimeLine(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        List<TimeDomain> timeLine =timeDao.getTimeLine();
        return new PageInfo<>(timeLine);
    }
}
