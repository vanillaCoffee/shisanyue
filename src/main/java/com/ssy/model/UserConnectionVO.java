package com.ssy.model;

import lombok.Data;

@Data
public class UserConnectionVO {
    private String displayName;
    private String imageUrl;
}
