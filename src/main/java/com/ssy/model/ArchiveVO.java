package com.ssy.model;

import com.ssy.domain.ContentDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ArchiveVO {
    private String date;
    private String count;
    private List<ContentDomain> articles;
}
