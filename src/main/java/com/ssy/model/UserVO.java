package com.ssy.model;

import lombok.Data;

@Data
public class UserVO {
    private String username;
    private String screenName;
    private String email;
}
