package com.ssy.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AttAchesVO {
    private Integer attachId;
    private String fileKey;
}
