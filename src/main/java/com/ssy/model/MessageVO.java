package com.ssy.model;

import lombok.Data;

@Data
public class MessageVO {
    private String url;
    private String author;
    private String content;
    private Integer created;
}
