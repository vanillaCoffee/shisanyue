package com.ssy.constant;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class WebConst {

    /**
     * 一些网站配置
     */
    public static Map<String, String> initConfig = new HashMap<>();


    /**
     * 记住我的时间
     */
    public static final int REMEMBER_ME_SECOND = 3600;


    /**
     * 最大获取文章条数
     */
    public static final int MAX_POSTS = 9999;

    /**
     * 最大页码
     */
    public static final int MAX_PAGE = 100;

    /**
     * 文章最多可以输入的文字数
     */
    public static final int MAX_TEXT_COUNT = 200000;

    /**
     * 文章标题最多可以输入的文字个数
     */
    public static final int MAX_TITLE_COUNT = 200;

    /**
     * 点击次数超过多少更新到数据库
     */
    public static final int HIT_EXCEED = 10;

    /**
     * 上传文件最大10M
     */
    public static Integer MAX_FILE_SIZE = 10485760;

}
