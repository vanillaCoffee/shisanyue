package com.ssy.constant;

import lombok.Data;


public enum ExceptionEnum {
    //Common
    PARAM_IS_EMPTY("参数为空"),
    INVALID_PARAM("无效的参数"),
    CAN_NOT_FIND_PARAM_TO_CONTIUNE("找不到参数继续运行"),

    //Article
    UPDATE_ARTICLE_FAIL("更新文章失败"),
    ADD_NEW_ARTICLE_FAIL("添加文章失败"),
    DELETE_ARTICLE_ERROR("删除文章失败"),
    TITLE_IS_TOO_LONG("文章标题过长"),
    TITLE_CAN_NOT_EMPTY("文章标题不能为空"),
    CONTENT_CAN_NOT_EMPTY("文章内容不能为空"),
    CONTENT_IS_TOO_LONG("文章字数超过限制"),
    CONTENT_IS_NOT_EXIST("该文章不存在"),

    //AttAch
    ADD_NEW_ATT_FAIL("添加附件信息失败"),
    UPDATE_ATT_FAIL("更新附件信息失败"),
    DELETE_ATT_FAIL("删除附件信息失败"),
    UPLOAD_FILE_FAIL("上传附件失败"),

    //Comment
    ADD_NEW_COMMENT_FAIL("添加评论失败"),
    UPDATE_COMMENT_FAIL("更新评论失败"),
    DELETE_COMMENT_FAIL("删除评论失败"),
    COMMENT_NOT_EXIST("评论不存在"),
    COMMENT_USER_NOT_LOGIN("请先登录后评论"),
    MESSAGE_USER_NOT_LOGIN("请先登录后留言"),

    //Option
    DELETE_OPTION_FAIL("删除配置失败"),
    UPDATE_OPTION_FAIL("更新配置失败"),

    //Meta
    ADD_META_FAIL("添加项目信息失败"),
    UPDATE_META_FAIL("更新项目信息失败"),
    DELETE_META_FAIL("删除项目信息失败"),
    NOT_ONE_RESULT("获取的项目的数量不止一个"),
    META_IS_EXIST("该项目已经存在"),

    //Auth
    USERNAME_PASSWORD_IS_EMPTY("用户名和密码不可为空"),
    USERNAME_PASSWORD_ERROR("用户名不存在或密码错误"),
    NOT_LOGIN("用户未登录");


    //错误码
    private Integer code;
    //提示信息
    private String message;

    //构造函数
    ExceptionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    ExceptionEnum(String message) {
        this.code = 1;
        this.message = message;
    }

    //获取状态码
    public Integer getCode() {
        return code;
    }

    //获取提示信息
    public String getMessage() {
        return message;
    }
}
