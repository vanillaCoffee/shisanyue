package com.ssy.utils;

import org.springframework.stereotype.Component;

import static com.ssy.utils.DateKit.fmtdate;

@Component
public class DateUtil {

    public static String fmtdate_default(Integer unixTime) {
        return fmtdate(unixTime, "yyyy-MM-dd");
    }

    public static String fmtdate_md(Integer unixTime) {
        return fmtdate(unixTime, "yyyy-MM");
    }
    /**
     * 英文格式的日期
     */
    public static String fmtdate_en(Integer unixTime) {
        String fmtdate = fmtdate(unixTime, "d,MMM,yyyy");
        String[] dateArr = fmtdate.split(",");
        return "<span>" + dateArr[0] + "</span> " + dateArr[1] + "  " + dateArr[2];
    }

    /**
     * 英文格式的日期-年
     */
    public static String fmtdate_en_y(Integer unixTime) {
        return fmtdate(unixTime, "yyyy");
    }

    /**
     * 英文格式的日期-月
     */
    public static String fmtdate_en_mo(Integer unixTime) {
        return fmtdate(unixTime, "MMM");
    }

    /**
     * 英文格式的日期-日
     */
    public static String fmtdate_en_d(Integer unixTime) {
        return fmtdate(unixTime, "d");
    }

}
