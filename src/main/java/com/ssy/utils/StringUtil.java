package com.ssy.utils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

@Component
public class StringUtil {
    public static String updateMeta(String name, String metas) {
        String[] ms = StringUtils.split(metas, ",");
        StringBuilder sbuf = new StringBuilder();
        for (String m : ms) {
            if (!name.equals(m)) {
                sbuf.append(",").append(m);
            }
        }
        if (sbuf.length() > 0) {
            return sbuf.substring(1);
        }
        return "";
    }

    /**
     * 数组转字符串
     */
    public static String join(String[] arr) {
        StringBuilder ret = new StringBuilder();
        int var4 = arr.length;
        for (String item : arr) {
            ret.append(',').append(item);
        }
        return ret.length() > 0 ? ret.substring(1) : ret.toString();
    }

    /**
     * 字符串转整型
     */
    public static Integer stringToInteger(String s) {
        return Integer.parseInt(s);
    }

    /**
     * QQ头像http转https
     */
    public static String httpImgToHttpsImg(String httpImg) {
        String prefix = "https";
        String[] s = StringUtils.split(httpImg, ":");
        return prefix + ':' + s[1];
    }

    /**
     * 字符串截取
     */
    public static String subString(String s) {
        String suffix = "...";
        String pattern = ".*img.*";
        boolean isMatch = Pattern.matches(pattern, s);
        if (!s.substring(3, 4).equalsIgnoreCase("<") && s.length() > 20)
            return s.substring(0, 15) + suffix;
        else
            return s;
    }
}
