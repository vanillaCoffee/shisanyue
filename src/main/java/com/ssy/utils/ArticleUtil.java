package com.ssy.utils;

import com.ssy.domain.MetaDomain;
import com.ssy.service.site.SiteService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class ArticleUtil {
    @Autowired
    private SiteService siteService;

    private static SiteService site;


    @PostConstruct
    public void init() {
        site = siteService;
    }

    /**
     * 判断category和文章类型的交集
     */
    public static boolean exist_cat(MetaDomain category, String cats) {
        String[] arr = StringUtils.split(cats, ",");
        if (null != arr && arr.length > 0) {
            for (String c : arr) {
                if (c.trim().equals(category.getMetaName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Long getArticleCommentCount(Integer contentId) {
        return site.getCommentsCountByContentId(contentId);
    }

}
