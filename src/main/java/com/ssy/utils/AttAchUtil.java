package com.ssy.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.InputStream;
import java.util.Date;

public class AttAchUtil {
    /**
     * 判断附件是否是图片
     */
    public static boolean isImage(InputStream imageFile) {
        try {
            Image img = ImageIO.read(imageFile);
            return img != null && img.getWidth(null) > 0 && img.getHeight(null) > 0;
        } catch (Exception e) {
            return false;
        }
    }

    public static String getFileName(String filename){
        String prefix = "/upload/" + DateKit.dateFormat(new Date(), "yyyy/MM") + '/';
        filename = prefix + filename;
        return filename;
    }
}
