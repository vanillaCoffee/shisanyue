package com.ssy.utils;

import org.springframework.stereotype.Component;

@Component
public class SiteUtil {
    /**
     * 返回blog文章地址
     */
    public static String blogPermalink(Integer cid) {
        if (cid !=null)
            return "/blog/article/" + cid.toString();
        else
            return "redirect:/admin/index";
    }

    public static String articleEditLink(Integer cid){
        if (cid !=null)
            return "/admin/article/" + cid.toString();
        else
            return "redirect:/admin/index";
    }
    /**
     * 返回作品文章地址
     */
    public static String photoPermalink(Integer cid) {
        if(cid !=null)
            return "/work/article/" + cid.toString();
        else
            return "redirect:/admin/index";
    }

    /**
     * 没有封面的文章/作品自动获取一张图片
     */
    public static String randomBlogPic() {
        return "/site/image/blog-images/blog-" + RandomUtil.random(12, ".jpg");
    }
}
