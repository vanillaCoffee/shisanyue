package com.ssy.utils;

import com.ssy.dto.CommentDto;
import com.ssy.dto.cond.UserConnectionCond;
import com.ssy.service.comment.CommentService;
import com.ssy.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Collection;

@Slf4j
@Component
public class UserUtil {
    @Resource
    private UserService userService;

    @Resource
    private CommentService commentService;

    private static UserService user;

    private static CommentService comment;


    @PostConstruct
    public void init() {
        user = userService;
        comment = commentService;
    }

    public static void addUserConnection(String accessToken, String openId) {
        UserConnectionCond userConnectionCond = new UserConnectionCond();
        userConnectionCond.setAccessToken(accessToken);
        userConnectionCond.setProviderUserId(openId);
        user.addUserConnection(userConnectionCond);
    }

    //判断QQ用户是否已经存在sys_userconnection表中
    public static boolean isExist(String providerUserId) {
        Collection collection = user.getProviderUserIdList();
        return collection.contains(providerUserId);
    }

    public static String getParentNickName(Integer parentId) {
        return comment.getParentNickNameByParentId(parentId);
    }

    public static boolean isowner(Integer parentId) {
        if (parentId == 0) {
            return false;
        } else {
            CommentDto commentDto = comment.getCommentsByCommentId(parentId);
            return commentDto.getIsowner() == 1;
        }
    }
}
