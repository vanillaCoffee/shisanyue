package com.ssy.utils;

import org.apache.commons.io.IOUtils;
import org.lionsoul.ip2region.*;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class AddressUtils {

    public static String IpSearch(String ip) throws DbMakerConfigException, IOException {
        String[] ipAddressArray;
        DbConfig config = new DbConfig();
        InputStream dbfile = AddressUtils.class.getResourceAsStream("/ip2region.db"); // 这个文件若没有请到以下地址下载：
        DbSearcher searcher = new DbSearcher(config, IOUtils.toByteArray(dbfile));
        DataBlock block2 = searcher.memorySearch(ip);
        String ipAddress = block2.getRegion();
        ipAddressArray = ipAddress.split("\\|");
        String country = ipAddressArray[0].equalsIgnoreCase("0") ? "中国" : ipAddressArray[0];
        String subdivision = ipAddressArray[2].equalsIgnoreCase("0") ? "" : ipAddressArray[2];
        String city = ipAddressArray[3].equalsIgnoreCase("0") ? "" : ipAddressArray[3];
        return country + subdivision + city;
    }



    public static void main(String[] args) throws DbMakerConfigException, IOException {
        String ip = "223.10.229.217";
        // 判断是否为IP地址
        boolean isIpAddress = Util.isIpAddress("12123.34"); // false
        isIpAddress = Util.isIpAddress(ip); // true

        // IP地址与long互转
        long ipLong = Util.ip2long(ip); // 794805406
        String strIp = Util.long2ip(ipLong); // 47.95.196.158

        // 根据IP搜索地址信息
        DbConfig config = new DbConfig();
        String dbfile = AddressUtils.class.getResource("/ip2region.db").getPath(); // 这个文件若没有请到以下地址下载：
        // https://gitee.com/lionsoul/ip2region/tree/master/data
        DbSearcher searcher = new DbSearcher(config, dbfile);

        // 二分搜索
        long start = System.currentTimeMillis();
        DataBlock block1 = searcher.binarySearch(ip);
        long end = System.currentTimeMillis();
        System.out.println(block1.getRegion()); // 中国|华东|浙江省|杭州市|阿里巴巴
        System.out.println("使用二分搜索，耗时：" + (end - start) + " ms"); // 1ms

        // B树搜索（更快）
        start = System.currentTimeMillis();
        DataBlock block2 = searcher.btreeSearch(ip);
        System.out.println(block2.getRegion());
        end = System.currentTimeMillis();
        System.out.println("使用B树搜索，耗时：" + (end - start) + " ms"); // 0ms

        System.out.println(AddressUtils.IpSearch(ip));
    }

}
