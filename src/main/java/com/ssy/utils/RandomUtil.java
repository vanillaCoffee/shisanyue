package com.ssy.utils;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class RandomUtil {
    private static final Random random = new Random();

    // 获取最小值到最大值之间的数
    public static int random(int min, int max) {
        return random.nextInt(max) % (max - min + 1) + min;
    }

    public static String random(int max, String str) {
        return random(1, max) + str;
    }

    private static final String[] COLORS = {"red", "orange", "green", "cyan", "blue", "black", "gray"};

    public static String rand_color() {
        Random random = new Random(7);
        int r = random(0, COLORS.length - 1);
        return COLORS[r];
    }
}
