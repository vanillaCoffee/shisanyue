package com.ssy.domain;

import lombok.Data;


@Data
public class UserDomain {
    private Integer userId;
    private String username;
    private String password;
    private String email;
    private String screenName;
    private Integer created;
    private boolean enabled;
    private boolean accountExpired;
    private boolean credentialsExpired;
    private boolean locked;
}
