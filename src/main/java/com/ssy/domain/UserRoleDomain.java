package com.ssy.domain;

import lombok.Data;

@Data
public class UserRoleDomain {
    private Integer userId;
    private Integer roleId;
}
