package com.ssy.domain;

import lombok.Data;

@Data
public class MenuDomain {
    private Integer menuId;
    private String menuName;
    private String url;
    private String icon;
    private String remark;
}
