package com.ssy.domain;

import lombok.Data;

@Data
public class MetaDomain {
    private Integer metaId;
    private String metaName;
    private String slug;
    private String metaType;
    private String contentType;
    private String icon;
    private String description;
    private Integer sort;
}
