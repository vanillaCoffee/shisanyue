package com.ssy.domain;

import lombok.Data;

@Data
public class AttAchDomain {
    private Integer attachId;
    private String fileName;
    private String fileType;
    private String fileKey;
    private Integer authorId;
    private Integer created;
}
