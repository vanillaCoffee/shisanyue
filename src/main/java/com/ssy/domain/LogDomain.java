package com.ssy.domain;

import lombok.Data;

@Data
public class LogDomain {
    private Integer logId;
    private String action;
    private String data;
    private Integer authorId;
    private String ip;
    private Integer created;
}
