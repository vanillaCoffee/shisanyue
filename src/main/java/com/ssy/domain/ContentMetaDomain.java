package com.ssy.domain;

import lombok.Data;

@Data
public class ContentMetaDomain {
    private Integer contentId;
    private Integer metaId;
}
