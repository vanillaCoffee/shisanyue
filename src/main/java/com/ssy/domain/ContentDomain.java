package com.ssy.domain;

import lombok.Data;

@Data
public class ContentDomain {
    private Integer contentId;
    private String title;
    private String titlePic;
    private String music;
    private Integer top;
    private String source;
    private String slug;
    private String created;
    private String modified;
    private String content;
    private Integer authorId;
    private String type;
    private String status;
    private String tags;
    private String categories;
    private Integer hits;
    private Integer commentsNum;
    private Integer allowComment;
}
