package com.ssy.domain;

import lombok.Data;

@Data
public class RoleDomain {
    private Integer roleId;
    private String roleName;
}
