package com.ssy.domain;

import lombok.Data;

@Data
public class OptionDomain {
    private String name;
    private String value;
    private String description;
}
