package com.ssy.domain;

import lombok.Data;

import java.util.List;

@Data
public class CommentDomain {
    private Integer commentId;
    private Integer contentId;
    private Integer created;
    private String author;
    private Integer authorId;
    private Integer ownerId;
    private String url;
    private String ip;
    private String headUrl;
    private String location;
    private String content;
    private String type;
    private String status;
    private Integer parent;
    private String email;
    private String toemail;
    private Integer isowner;
}
