package com.ssy.domain;

import lombok.Data;

@Data
public class RoleMenuDomain {
    private Integer roleId;
    private Integer menuId;
}
