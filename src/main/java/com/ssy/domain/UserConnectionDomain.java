package com.ssy.domain;

import lombok.Data;

@Data
public class UserConnectionDomain {
    private String userId;
    private String providerId;
    private String providerUserId;
    private Integer rank;
    private String displayName;
    private String profileUrl;
    private String imageUrl;
    private String accessToken;
    private String secret;
    private String refreshToken;
    private String expireTime;
    private String email;
}
