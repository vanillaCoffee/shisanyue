package com.ssy.domain;

import lombok.Data;

@Data
public class TimeDomain {
    private Integer timeId;
    private String content;
    private String status;
    private Integer created;
}
