package com.ssy.oauth2;

import com.ssy.utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;
@Slf4j
public class QQApiAdapter implements ApiAdapter<QQApi> {

    @Override
    public boolean test(QQApi qqApi) {
        return true;
    }

    @Override
    public void setConnectionValues(QQApi api, ConnectionValues values) {
        QQUser user = api.getUserInfo();
//        log.info("第三方用户详情"+user);
        values.setDisplayName(user.getNickname());
        values.setImageUrl(StringUtil.httpImgToHttpsImg(user.getFigureurl_qq_2()));
        values.setProviderUserId(user.getOpenId());
        values.setProfileUrl(user.getOpenId());
    }

    @Override
    public UserProfile fetchUserProfile(QQApi qqApi) {
        return null;
    }

    @Override
    public void updateStatus(QQApi qqApi, String s) {

    }
}
