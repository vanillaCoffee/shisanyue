package com.ssy.oauth2;

import org.springframework.social.oauth2.AbstractOAuth2ServiceProvider;

public class QQServiceProvider extends AbstractOAuth2ServiceProvider<QQApi> {
    //OAuth2获取授权码的请求地址
    private static final String URL_AUTHORIZE = "https://graph.qq.com/oauth2.0/authorize";

    //OAuth2获取AccessToken的请求地址
    private static final String URL_GET_ACCESS_TOKEN = "https://graph.qq.com/oauth2.0/token";

    private String appId;

    //负责认证流程请求响应
    public QQServiceProvider(String appId, String appSecret) {
        super(new QQOAuth2Template(appId, appSecret, URL_AUTHORIZE, URL_GET_ACCESS_TOKEN));
        this.appId = appId;
    }

    //负责资源请求响应
    @Override
    public QQApi getApi(String accessToken) {
        return new QQApiImpl(accessToken, appId);
    }
}
