package com.ssy.oauth2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ssy.config.security.MyUserDetailService;
import com.ssy.utils.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.social.connect.Connection;
import org.springframework.social.oauth2.AbstractOAuth2ApiBinding;
import org.springframework.social.oauth2.TokenStrategy;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Slf4j
public class QQApiImpl extends AbstractOAuth2ApiBinding implements QQApi {


    private static final String URL_GET_OPENID = "https://graph.qq.com/oauth2.0/me?access_token=%s";

    private static final String URL_GET_USERINFO = "https://graph.qq.com/user/get_user_info?oauth_consumer_key=%s&openid=%s";

    private String appId;

    private String openId;

    private ObjectMapper objectMapper = new ObjectMapper();

    public static String allOpenId;

    public QQApiImpl(String accessToken, String appId) {
        //默认是使用header传递accessToken，而QQ比较特殊是用parameter传递token
        super(accessToken, TokenStrategy.ACCESS_TOKEN_PARAMETER);

        this.appId = appId;
        this.openId = getOpenId(accessToken);
        allOpenId = this.openId;
        if (!UserUtil.isExist(openId)){
            UserUtil.addUserConnection(accessToken,openId);
        }
//        log.info("QQ互联平台openId:{}", this.openId);
    }

    //通过接口获取openId
    private String getOpenId(String accessToken) {
        String url = String.format(URL_GET_OPENID, accessToken);
        String result = getRestTemplate().getForObject(url, String.class);
        return StringUtils.substringBetween(result, "\"openid\":\"", "\"}");
    }

    @Override
    public QQUser getUserInfo() {
        try {
            String url = String.format(URL_GET_USERINFO, appId, openId);
            String result = getRestTemplate().getForObject(url, String.class);
            QQUser userInfo = objectMapper.readValue(result, QQUser.class);
            userInfo.setOpenId(openId);
            return userInfo;
        } catch (Exception e) {
            throw new RuntimeException("获取用户信息失败", e);
        }
    }

}
