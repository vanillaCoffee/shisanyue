package com.ssy.oauth2;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

/**
 * QQ用户信息
 */
//因为我们定义的信息不完整，为了避免映射字段找不到的异常
@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class QQUser {
    private String openId;
    //返回码：0表示获取成功
    private String ret;
    //返回错误信息，如果返回成功，错误信息为空串
    private String msg;
//    //省(直辖市)
//    private String province;
//    //市(直辖市区)
//    private String city;
    //用户昵称
    private String nickname;
    //用户的头像100*100
    private String figureurl_qq_2;
    //性别
    private String gender;
}
