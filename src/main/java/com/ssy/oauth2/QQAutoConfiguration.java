package com.ssy.oauth2;

import com.ssy.config.social.jdbc.SsyJdbcUsersConnectionRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurerAdapter;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.security.AuthenticationNameUserIdSource;
import org.springframework.social.security.SpringSocialConfigurer;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * AbstractOAuth2ApiBinding封装了accessToken以及RestTemplate，
 * 帮助我们实现HTTP请求的参数携带，以及请求结果到对象的反序列化工作等。
 */
@Configuration
@EnableSocial
public class QQAutoConfiguration extends SocialConfigurerAdapter {
    @Resource
    private DataSource dataSource;
    @Value("${thirdlogin.providerId}")
    private String providerId;
    @Value("${thirdlogin.appId}")
    private String appId;
    @Value("${thirdlogin.appSecret}")
    private String appSecret;

    @Bean
    public SpringSocialConfigurer qqFilterConfig() {
        QQFilterConfigurer configurer = new QQFilterConfigurer("/auth");
//        configurer.signupUrl("/qqbind");         //绑定用户界面;
        configurer.postLoginUrl("/index");
        return configurer;
    }

    @Bean
    public ProviderSignInUtils providerSignInUtils(ConnectionFactoryLocator connectionFactoryLocator) {
        return new ProviderSignInUtils(connectionFactoryLocator,
                getUsersConnectionRepository(connectionFactoryLocator)) {
        };
    }

    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        SsyJdbcUsersConnectionRepository usersConnectionRepository =
                new SsyJdbcUsersConnectionRepository(dataSource, connectionFactoryLocator, Encryptors.noOpText());
        // 设置表前缀
        usersConnectionRepository.setTablePrefix("sys_");
        return usersConnectionRepository;
    }

    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer connectionFactoryConfigurer,
                                       Environment environment) {
        connectionFactoryConfigurer.addConnectionFactory(new QQConnectionFactory(providerId, appId, appSecret));
    }


    @Override
    public UserIdSource getUserIdSource() {
        return new AuthenticationNameUserIdSource();
    }
}
