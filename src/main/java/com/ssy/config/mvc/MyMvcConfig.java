package com.ssy.config.mvc;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyMvcConfig implements WebMvcConfigurer {
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/about").setViewName("site/about");
        registry.addViewController("/music").setViewName("site/music");
        registry.addViewController("/bind").setViewName("site/bind");
    }
}
