package com.ssy.config.security;

import com.ssy.constant.WebConst;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.social.security.SpringSocialConfigurer;

import javax.annotation.Resource;
import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private DataSource dataSource;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private SuccessAuthenticationHandler successAuthenticationHandler;

    @Autowired
    private FailAuthenticationHandler failAuthenticationHandler;

    @Resource
    private SpringSocialConfigurer qqFilterConfig;

    @Autowired
    private MyLogoutSuccessHandler myLogoutSuccessHandler;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * 设置记住我组件
     */
    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
        tokenRepository.setDataSource(dataSource);
        return tokenRepository;
    }


    @Bean
    public ServletListenerRegistrationBean<HttpSessionEventPublisher> httpSessionEventPublisher() {
        return new ServletListenerRegistrationBean<>(new HttpSessionEventPublisher());
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    //授权
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //请求授权的规则(拦截器)
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/toLogin").permitAll()
                .antMatchers("/admin/js/**", "/admin/css/**", "/admin/images", "/lib/**").permitAll()
                .antMatchers("/admin/index/**").hasRole("admin")
                .antMatchers("/admin/article/**").hasRole("admin")
                .antMatchers("/admin/timeline/**").hasRole("admin")
                .antMatchers("/admin/article/**").hasRole("admin")
                .antMatchers("/admin/article/manage/**").hasRole("admin")
                .antMatchers("/admin/article/publish/**").hasRole("admin")
                .antMatchers("/admin/fileUp/**").hasRole("admin")
                .antMatchers("/admin/category/**").hasRole("admin")
                .antMatchers("/admin/attach/**").hasRole("admin")
                .antMatchers("/admin/profile/**").hasRole("admin")
                .antMatchers("/admin/comments/**").hasRole("admin")
                .antMatchers("/admin/links/**").hasRole("admin")
                .antMatchers("/admin/setting/**").hasRole("admin");
//                .antMatchers("/comment/add").hasRole("user")
//                .antMatchers("/email/add").hasRole("user");

        //采用formLogin登录
        http.formLogin()
                .loginPage("/toLogin")   //指定登录页面的url
                .loginProcessingUrl("/admin/login") // 让springsecurity的UsernamePassword过滤器支持处理/admin/login请求(获取username和password)
                .successHandler(successAuthenticationHandler)
                .failureHandler(failAuthenticationHandler);

        //禁用csrf拦截器
        http.csrf().disable();

        //设置注销后清除cookies
        http.logout().deleteCookies("JSESSIONID").logoutSuccessUrl("/index");
                //自定义logoutSuccessHandler
//                .logoutSuccessHandler(myLogoutSuccessHandler);

        //设置qq拦截器配置
        http.apply(qqFilterConfig);

        //解决不能加载iframe组件的问题
        http.headers().frameOptions().disable();

        //设置记住我
        http.rememberMe()
                .tokenRepository(persistentTokenRepository())
                .tokenValiditySeconds(WebConst.REMEMBER_ME_SECOND)
                .userDetailsService(userDetailsService);


        //session管理
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .invalidSessionUrl("/index")    //session失效返回页面
                .sessionFixation().migrateSession()   //springsecurity默认session重发配置，每一次重置session值，重发JSESSIONID
                .maximumSessions(1000);   //设置最大session数
//                .maxSessionsPreventsLogin(false)  //单用户登录，如果有一个登录了，同一个用户在其他地方登录将前一个剔除下线
//                .expiredSessionStrategy(new MyExpiredSessionStrategy());


    }
}
