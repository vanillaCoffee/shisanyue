package com.ssy.config.security;

import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.log.LogService;
import com.ssy.service.user.UserService;
import com.ssy.utils.FastJsonUtil;
import es.moki.ratelimitj.core.limiter.request.RequestLimitRule;
import es.moki.ratelimitj.core.limiter.request.RequestRateLimiter;
import es.moki.ratelimitj.inmemory.request.InMemorySlidingWindowRequestRateLimiter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Configuration
public class FailAuthenticationHandler extends SimpleUrlAuthenticationFailureHandler {
    @Value("${spring.security.loginType}")
    private String loginType;

    @Autowired
    MyUserDetailService myUserDetailService;

    @Autowired
    private UserService userService;

    @Autowired
    private LogService logService;

    private Set<RequestLimitRule> rules = Collections.singleton(RequestLimitRule.of(60, TimeUnit.MINUTES,2));
    private RequestRateLimiter limiter = new InMemorySlidingWindowRequestRateLimiter(rules);

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response,
                                        AuthenticationException e)
            throws IOException, ServletException {
        String username = request.getParameter("username");
        String errorMsg = "用户名或密码输入错误";

        if (e instanceof LockedException){
            errorMsg = "您已经2次登陆失败，账户已被锁定，请稍后再试！";
        }else if (e instanceof AccountExpiredException){
            errorMsg = "您的账户已过期，请联系管理员！";
        }else if (e instanceof CredentialsExpiredException){
            errorMsg = "您的证书已过期";
        }else if (e instanceof DisabledException){
            errorMsg = "用户已被禁用";
        } else {
            boolean reachLimit = limiter.overLimitWhenIncremented(username);
            if (reachLimit){
                MyUserDetails user = (MyUserDetails)myUserDetailService.loadUserByUsername(username);
                user.setLocked(false);
                userService.updateUserAccountInfo(user);
            }
        }
        if (e instanceof SessionAuthenticationException){
            errorMsg = e.getMessage();
        }
        if (loginType.equalsIgnoreCase("JSON")){
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(FastJsonUtil.toJSONString(LayUiJsonResponse.fail(errorMsg)));
        }else {
            // 使用父类方法，跳转到登录页面
            super.onAuthenticationFailure(request,response,e);
        }
    }
}
