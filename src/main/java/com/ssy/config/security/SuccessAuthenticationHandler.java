package com.ssy.config.security;


import com.ssy.dao.UserDao;
import com.ssy.dto.UserDto;
import com.ssy.constant.LogActions;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.log.LogService;
import com.ssy.utils.FastJsonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
public class SuccessAuthenticationHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    @Value("${spring.security.loginType}")
    private String loginType;

    @Autowired
    private LogService logService;

    @Autowired
    private UserDao userDao;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication)
            throws IOException, ServletException {
        if (loginType.equalsIgnoreCase("JSON")){
            response.setContentType("application/json;charset=UTF-8");
            response.getWriter().write(FastJsonUtil.toJSONString(LayUiJsonResponse.success("登录成功")));
            MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            UserDto userDto = userDao.getUserInfoByUsernameAndSysUser(myUserDetails.getUsername());
            logService.addLog(LogActions.LOGIN.getAction(), null, request.getRemoteAddr(), userDto.getUserId());
        }else {
            // 使用父类方法，跳转到登录之前的页面
            super.onAuthenticationSuccess(request,response,authentication);
            MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            UserDto userDto = userDao.getUserInfoByUsernameAndSysUser(myUserDetails.getUsername());
            logService.addLog(LogActions.LOGIN.getAction(), null, request.getRemoteAddr(), userDto.getUserId());
        }
    }
}
