package com.ssy.config.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.social.security.SocialUserDetails;

import java.util.Collection;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyUserDetails implements SocialUserDetails {
    String password;  //密码
    String username;  //用户名
//    String displayName;
//    String imageUrl;
    boolean accountExpired;   //是否过期
    boolean credentialsExpired;   //是否密码过期
    boolean locked;  //是否被锁定
    boolean enabled;  //账号是否可用
    Collection<? extends GrantedAuthority> authorities;  //用户的权限集合

    @Override
    public String getUserId() {
        return username;
    }


    @Override
    public boolean isAccountNonExpired() {
        return accountExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return credentialsExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setLocked(boolean locked) {
        this.locked = locked;
    }
}
