package com.ssy.config.security;

import com.ssy.dao.RoleMenuDao;
import com.ssy.dao.UserDao;
import com.ssy.dao.UserRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUserDetails;
import org.springframework.social.security.SocialUserDetailsService;

import java.util.List;
import java.util.stream.Collectors;

@Configuration
public class MyUserDetailService implements UserDetailsService, SocialUserDetailsService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private UserRoleDao userRoleDao;

    @Autowired
    private RoleMenuDao roleMenuDao;



    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        return getMyUserDetails(username);
    }

    @Override
    public SocialUserDetails loadUserByUserId(String userId) throws UsernameNotFoundException {
        return getMyUserDetails(userId);
    }

    private MyUserDetails getMyUserDetails(String username) {
        //加载基础用户信息
        MyUserDetails myUserDetails = userDao.getUserInfoByUsername(username);


        //加载用户角色列表
        List<String> roleCodes = userRoleDao.getRoleByRelationShipAndUsername(username);


        //通过用户角色列表加载用户的资源权限列表
        List<String> authorities = roleMenuDao.getMenuByRelationShipAndRoleList(roleCodes);

        //角色是一个特殊的权限，ROLE_前缀
        roleCodes = roleCodes.stream()
                .map(rc -> "ROLE_" +rc)
                .collect(Collectors.toList());

        authorities.addAll(roleCodes);

        myUserDetails.setAuthorities(
                AuthorityUtils.commaSeparatedStringToAuthorityList(
                        String.join(",",authorities)
                )
        );
        return myUserDetails;
    }
}
