package com.ssy.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LayUiJsonResponse {
    private Integer code;
    private String msg;
    private Long count;
    private Object date;

    public static LayUiJsonResponse SERVER_ERROR = LayUiJsonResponse.fail(0,"服务端异常");
    public static LayUiJsonResponse BIND_ERROR = LayUiJsonResponse.fail(0,"网络错误");

    public static LayUiJsonResponse success() {
        LayUiJsonResponse response = new LayUiJsonResponse();
        response.setCode(0);
        response.setMsg("success");
        return response;
    }

    public static LayUiJsonResponse fail(int code,String msg) {
        LayUiJsonResponse response = new LayUiJsonResponse();
        response.setCode(code);
        response.setMsg("success");
        return response;
    }

    public static LayUiJsonResponse fail() {
        LayUiJsonResponse response = new LayUiJsonResponse();
        response.setCode(1);
        response.setMsg("fail");
        return response;
    }

    public static LayUiJsonResponse success(String msg) {
        LayUiJsonResponse response = new LayUiJsonResponse();
        response.setCode(0);
        response.setMsg(msg);
        return response;
    }

    public static LayUiJsonResponse fail(String msg) {
        LayUiJsonResponse response = new LayUiJsonResponse();
        response.setCode(1);
        response.setMsg(msg);
        return response;
    }

    public static LayUiJsonResponse fail(Object date) {
        LayUiJsonResponse response = new LayUiJsonResponse();
        response.setCode(1);
        response.setDate(date);
        return response;
    }

    public static LayUiJsonResponse success(String msg, List<Map<String,String>> list) {
        LayUiJsonResponse response = new LayUiJsonResponse();
        response.setCode(0);
        response.setMsg(msg);
        response.setDate(list);
        return response;
    }

    public static LayUiJsonResponse success(String msg, Object date) {
        LayUiJsonResponse response = new LayUiJsonResponse();
        response.setCode(0);
        response.setMsg(msg);
        response.setDate(date);
        return response;
    }

    public static LayUiJsonResponse success(Object date) {
        LayUiJsonResponse response = new LayUiJsonResponse();
        response.setCode(0);
        response.setDate(date);
        return response;
    }



    public LayUiJsonResponse fillArgs(Object... args) {
        int code = this.code;
        String message = String.format(this.msg, args);
        return LayUiJsonResponse.fail(code, message);
    }
}
