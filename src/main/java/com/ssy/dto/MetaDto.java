package com.ssy.dto;

import com.ssy.domain.MetaDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MetaDto extends MetaDomain {
    private int count;
}
