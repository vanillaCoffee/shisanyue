package com.ssy.dto.cond;

import lombok.Data;

@Data
public class EmailCond {
    private String providerUserId;
    private String email;
}
