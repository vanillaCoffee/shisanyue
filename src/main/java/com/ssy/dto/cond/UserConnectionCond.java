package com.ssy.dto.cond;

import lombok.Data;

@Data
public class UserConnectionCond {
    private String userId = "user";
    private String providerId = "qq";
    private String providerUserId;
    private String accessToken;

}
