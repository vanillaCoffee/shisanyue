package com.ssy.dto.cond;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAccountCond {
    private int userId;
    private boolean enabled;
    private boolean accountExpired;
    private boolean credentialsExpired;
    private boolean locked;
}
