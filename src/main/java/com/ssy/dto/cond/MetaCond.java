package com.ssy.dto.cond;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MetaCond {
    private Integer metaId;
    private String metaName;
    private String metaType;
}
