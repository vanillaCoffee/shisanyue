package com.ssy.dto.cond;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户查找条件
 * Created by JiaWei.Zhang on 2020/1/1.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserCond {
    private Integer userId;
    private String username;
    private String password;
    private String screenName;
    private String email;
}
