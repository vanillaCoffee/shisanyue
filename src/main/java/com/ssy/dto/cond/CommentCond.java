package com.ssy.dto.cond;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 评论的查找参数
 * Created by JiaWei.Zhang on 2020/1/1.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentCond {
    /**
     * 状态
     */
    private String status;
    /**
     * 类型
     */
    private String type;
    /**
     * 文章号
     */
    private Integer contentId;
    /**
     * 开始时间戳
     */
    private Integer startTime;
    /**
     * 结束时间戳
     */
    private Integer endTime;

    /**
     * 父评论编号
     */
    private Integer parent;
}
