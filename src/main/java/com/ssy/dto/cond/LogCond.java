package com.ssy.dto.cond;

import lombok.Data;

@Data
public class LogCond {
    private String action;
    private String data;
    private Integer authorId;
    private String ip;
}
