package com.ssy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 后台统计对象
 * Created by JiaWei.Zhang on 2020/1/1.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StatisticsDto {

    /**
     * 文章数
     */
    private Long articles;
    /**
     * 评论数
     */
    private Long comments;
    /**
     * 连接数
     */
    private Long links;
    /**
     * 附件数
     */
    private Long attachs;
}
