package com.ssy.dto;

import com.ssy.domain.CommentDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommentDto extends CommentDomain {

    private List<CommentDto> children = new ArrayList<>();

}
