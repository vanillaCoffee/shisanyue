package com.ssy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttAchDto {
    private String username;
    private Integer attachId;
    private String fileName;
    private String fileType;
    private String fileKey;
    private String authorId;
    private Integer created;
}
