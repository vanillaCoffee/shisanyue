package com.ssy.exception;

import com.ssy.response.LayUiJsonResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@RestControllerAdvice
public class ControllerExceptionAdvice {
    final static Logger LOG = LoggerFactory.getLogger(ControllerExceptionAdvice.class);

    @ExceptionHandler(value = Exception.class)
    public LayUiJsonResponse exceptionHandler(HttpServletRequest request, Exception e) {
        e.printStackTrace();
        if (e instanceof BusinessException) {
            BusinessException exception = (BusinessException) e;
            System.out.println("捕获到异常" + e.getMessage());
            return LayUiJsonResponse.fail(e.getMessage());
        } else if (e instanceof BindException) {
            BindException ex = (BindException) e;
            List<ObjectError> errors = ex.getAllErrors();
            ObjectError error = errors.get(0);
            String msg = error.getDefaultMessage();
            return LayUiJsonResponse.BIND_ERROR.fillArgs(msg);
        } else {
            return LayUiJsonResponse.SERVER_ERROR;
        }
    }

}
