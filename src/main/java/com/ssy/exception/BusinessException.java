package com.ssy.exception;

import com.ssy.constant.ExceptionEnum;
import com.ssy.response.LayUiJsonResponse;
import lombok.Data;

//自定义异常
@Data
public class BusinessException extends RuntimeException {
    private ExceptionEnum exceptionEnum;

    public BusinessException(ExceptionEnum exceptionEnum) {
        super(Enum.valueOf(ExceptionEnum.class,exceptionEnum.toString()).getMessage());
        this.exceptionEnum = exceptionEnum;
    }
}
