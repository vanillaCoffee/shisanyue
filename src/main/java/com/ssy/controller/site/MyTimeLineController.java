package com.ssy.controller.site;

import com.ssy.domain.TimeDomain;
import com.ssy.service.timeline.TimeLineService;
import com.ssy.service.user.UserService;
import com.ssy.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/diary")
public class MyTimeLineController {

    @Autowired
    private TimeLineService timeLineService;

    @Autowired
    private DateUtil dateFormat;
    @Autowired
    private UserService userService;
    @Autowired
    private ProviderSignInUtils providerSignInUtils;

    @GetMapping("")
    public String myTimeline(HttpServletRequest request) {
        List<TimeDomain> timeDomainList = timeLineService.getTimeLineList();
        Collections.reverse(timeDomainList);

        request.setAttribute("timeline", timeDomainList);
        request.setAttribute("date", dateFormat);
        return "site/diary";
    }
}
