package com.ssy.controller.site;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.ContentDomain;
import com.ssy.dto.cond.ContentCond;
import com.ssy.constant.Types;
import com.ssy.constant.WebConst;
import com.ssy.model.UserConnectionVO;
import com.ssy.service.content.ContentService;
import com.ssy.service.user.UserService;
import com.ssy.utils.ArticleUtil;
import com.ssy.utils.DateUtil;
import com.ssy.utils.SiteUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@Controller
public class WorkController {
    @Autowired
    private ContentService contentService;

    @Autowired
    private DateUtil date;

    @Autowired
    private SiteUtil siteUtil;

    @Autowired
    private ArticleUtil articleUtil;

    @Autowired
    private UserService userService;


    @GetMapping(value = "/work")
    public String index(
//            @PathVariable(name = "p")
            @RequestParam(name = "p", required = false, defaultValue = "1")
                    int page,
            @RequestParam(name = "limit", required = false, defaultValue = "9999")
                    int limit,
            HttpServletRequest request
    ) {
        page = page < 0 || page > WebConst.MAX_PAGE ? 1 : page;
        ContentCond contentCond = new ContentCond();
        contentCond.setType(Types.PHOTO.getType());
        PageInfo<ContentDomain> articles = contentService.getArticleByCond(contentCond, page, limit);
        Collections.reverse(articles.getList());
        List<UserConnectionVO> userConnectionVOList = userService.getUserHeadOrderByTime();

        request.setAttribute("archives", articles);
        request.setAttribute("date", date);
        request.setAttribute("site", siteUtil);
        request.setAttribute("articleUtil", articleUtil);
        request.setAttribute("active", "work");
        request.setAttribute("userInfo", userConnectionVOList);
        return "site/work";
    }
}
