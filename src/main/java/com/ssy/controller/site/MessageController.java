package com.ssy.controller.site;

import com.ssy.dao.UserDao;
import com.ssy.dto.CommentDto;
import com.ssy.dto.UserDto;
import com.ssy.dto.cond.CommentCond;
import com.ssy.config.security.MyUserDetails;
import com.ssy.constant.Types;
import com.ssy.service.comment.CommentService;
import com.ssy.utils.ArticleUtil;
import com.ssy.utils.DateUtil;
import com.ssy.utils.StringUtil;
import com.ssy.utils.UserUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
@Slf4j
@Controller
public class MessageController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private StringUtil stringUtil;

    @Autowired
    private CommentService commentService;

    @Autowired
    private DateUtil dateUtil;

    @Autowired
    private ArticleUtil articleUtil;

    @Autowired
    private UserUtil userUtil;



    @GetMapping("/message")
    public String getMessage(HttpServletRequest request) {
        if (request.getUserPrincipal() != null) {
            MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            UserDto userDto = userDao.getUserInfoByUsernameAndSysUser(myUserDetails.getUsername());
            request.setAttribute("user", userDto);
        }
        CommentCond commentCond = new CommentCond();
        commentCond.setType(Types.MESSAGE.getType());
        commentCond.setParent(0);
        List<CommentDto> comments = commentService.getComments(commentCond);
        Collections.reverse(comments);

        request.setAttribute("stringUtil", stringUtil);
        request.setAttribute("date", dateUtil);
        request.setAttribute("commentList", comments);
        request.setAttribute("articleUtil", articleUtil);
        request.setAttribute("userUtil", userUtil);


        return "site/message";
    }
}
