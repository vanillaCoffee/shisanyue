package com.ssy.controller.site;

import com.ssy.dto.cond.EmailCond;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.user.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
@Controller
public class ThirdUserController {
//    @Autowired
//    private ProviderSignInUtils providerSignInUtils;
    @Autowired
    private UserService userService;

//    @GetMapping("/qqbind")
//    public String regist(HttpServletRequest request){
//        HttpSession session = request.getSession();
//        Connection connection = providerSignInUtils.getConnectionFromSession(new ServletWebRequest(request));
//        session.setAttribute("qqImage",connection.getImageUrl());
//        session.setAttribute("qqName",connection.getDisplayName());
//        session.setAttribute("providerUserId",connection.getProfileUrl());
//        providerSignInUtils.doPostSignUp("user",new ServletWebRequest(request));
//        return "redirect:/auth/qq";
//    }
//
//    @GetMapping("/qq/logout")
//    public String logout(HttpServletRequest request){
//        HttpSession session = request.getSession();
//        String providerUserId = (String) session.getAttribute("providerUserId");
//        log.info("openid"+providerUserId);
//        userService.setUserIdNull(providerUserId);
//        return "redirect:/index";
//    }
    @ResponseBody
    @PostMapping("/email/add")
    public LayUiJsonResponse emailFall(HttpServletRequest request,
                                       @RequestParam(name = "providerUserId", required = true)
                                               String providerUserId,
                                       @RequestParam(name = "email", required = true)
                                               String email){
        HttpSession session = request.getSession();
        EmailCond emailCond = new EmailCond();
        emailCond.setProviderUserId(providerUserId);
        emailCond.setEmail(email);
        userService.addEmailByProviderUserId(emailCond);
        session.setAttribute("email",email);
        return LayUiJsonResponse.success("填补邮箱成功");
    }
}
