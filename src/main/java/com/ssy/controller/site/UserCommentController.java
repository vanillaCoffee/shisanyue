package com.ssy.controller.site;

import com.ssy.dto.CommentDto;
import com.ssy.constant.ExceptionEnum;
import com.ssy.constant.Types;
import com.ssy.exception.BusinessException;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.comment.CommentService;
import com.ssy.service.content.ContentService;
import com.ssy.service.user.UserService;
import com.ssy.utils.AddressUtil;
import com.ssy.utils.AddressUtils;
import com.ssy.utils.IPUtil;
import com.ssy.utils.XssUtil;
import lombok.extern.slf4j.Slf4j;
import org.lionsoul.ip2region.DbMakerConfigException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Slf4j
@Controller
@RequestMapping("/comment")
public class UserCommentController {
    @Autowired
    private CommentService commentService;

    @Autowired
    private ContentService contentService;

    @Resource
    private ProviderSignInUtils providerSignInUtils;

    @Resource
    private UserService userService;


    @PostMapping("/add")
    @ResponseBody
    public LayUiJsonResponse subComment(
            HttpServletRequest request,
            @RequestParam(name = "articleId", required = false)
                    Integer articleId,
            @RequestParam(name = "ownerId", required = true)
                    Integer ownerId,
            @RequestParam(name = "url", required = false)
                    String url,
            @RequestParam(name = "comment", required = true)
                    String content,
            @RequestParam(name = "toemail", required = false)
                    String toemail,
            @RequestParam(name = "email", required = true)
                    String email,
            @RequestParam(name = "parent", required = false, defaultValue = "0")
                    Integer parent,
            @RequestParam(name = "isowner", required = false, defaultValue = "0")
                    Integer isowner
    ) throws IOException, DbMakerConfigException {
        if (ownerId == 0 & articleId != null)
            throw new BusinessException(ExceptionEnum.COMMENT_USER_NOT_LOGIN);
        if (ownerId == 0 & articleId == null)
            throw new BusinessException(ExceptionEnum.MESSAGE_USER_NOT_LOGIN);
        HttpSession session = request.getSession();
//        log.info("QQ昵称"+session.getAttribute("qqName"));
        CommentDto comment = new CommentDto();
        CommentDto parentComment = new CommentDto();
        parentComment.setCommentId(parent);
        comment.setContentId(articleId);
        comment.setOwnerId(ownerId);
        comment.setContent(XssUtil.filterXSS(content));
        comment.setAuthorId(1);
        comment.setAuthor((String) session.getAttribute("qqName"));
        comment.setUrl(url);
        comment.setIp(IPUtil.getIPAddress(request));
        comment.setHeadUrl((String) session.getAttribute("qqImage"));
        comment.setParent(parent);
        comment.setLocation(AddressUtils.IpSearch(IPUtil.getIPAddress(request)));
//        comment.setBlog(contentService.getArticleByContentId(articleId));
//        comment.setParentComment(parentComment);
//        System.out.println(AddressUtil.getAddresses("ip='" + request.getRemoteAddr() + "'", "UTF-8"));
//        commentDomain.setLocation(AddressUtil.getAddresses("ip='" + request.getRemoteAddr() + "'", "UTF-8"));
        if (email.equals("")) {
            return LayUiJsonResponse.fail(2, "请认证邮箱");
        } else {
            comment.setEmail(email);
        }
        comment.setToemail(toemail);
        comment.setIsowner(isowner);
        if (articleId != null) {
            comment.setType(Types.COMMENT.getType());
            int res = commentService.addComment(comment);
            if (res > 0) {
                return LayUiJsonResponse.success("评论成功!");
            } else {
                return LayUiJsonResponse.fail("评论失败");
            }
        } else {
            comment.setType(Types.MESSAGE.getType());
            int res = commentService.addMessage(comment);
            if (res > 0) {
                return LayUiJsonResponse.success("留言成功!");
            } else {
                return LayUiJsonResponse.fail("留言失败");
            }
        }
    }
}
