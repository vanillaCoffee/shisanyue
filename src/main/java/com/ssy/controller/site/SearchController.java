package com.ssy.controller.site;

import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.content.ContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class SearchController {
    @Autowired
    private ContentService contentService;

    @GetMapping("/search/{keyword}")
    @ResponseBody
    public LayUiJsonResponse getSearchListByKeyWord(@PathVariable String keyword) {
        if (contentService.getSearchListByKeyWord(keyword).size() > 0) {
            return LayUiJsonResponse.success(contentService.getSearchListByKeyWord(keyword));
        } else if (keyword == null){
            return LayUiJsonResponse.fail();
        }else {
            return LayUiJsonResponse.fail();
        }
    }
}
