package com.ssy.controller.site;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.MetaDomain;
import com.ssy.dto.cond.MetaCond;
import com.ssy.constant.Types;
import com.ssy.constant.WebConst;
import com.ssy.service.meta.MetaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

@Controller
public class LinkController {
    @Autowired
    private MetaService metaService;

    @GetMapping(value = "/link")
    public String getLink(
            HttpServletRequest request,
//            @PathVariable("p")
            @RequestParam(name = "p", required = false, defaultValue = "1")
                    int p,
            @RequestParam(name = "limit", required = false, defaultValue = "9999")
                    int limit) {
        p = p < 0 || p > WebConst.MAX_PAGE ? 1 : p;
        MetaCond metaCond = new MetaCond();
        metaCond.setMetaType(Types.LINK.getType());
        PageInfo<MetaDomain> categories = metaService.getMetasByCondWithPage(metaCond, p, limit);
        Collections.reverse(categories.getList());
        request.setAttribute("categories", categories);
        request.setAttribute("length", categories.getList().size());
        return "site/link";
    }
}
