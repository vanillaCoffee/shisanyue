package com.ssy.controller.site;

import com.ssy.utils.EmailTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class MailController {

    @Autowired
    private EmailTool emailTool;

    @PostMapping("/sendmail")
    @ResponseBody
    public String toMail(
            HttpServletRequest request,
            @RequestParam(name = "articleId", required = false)
                    Integer articleId,
            @RequestParam(name = "toemail", required = false,defaultValue = "474888698@qq.com")
                    String toemail,
            @RequestParam(name = "email", required = true)
                    String email,
            @RequestParam(name = "qqName", required = true)
                    String qqName,
            @RequestParam(name = "qqImage", required = true)
                    String qqImage,
            @RequestParam(name = "comment", required = true)
                    String content
    ) throws MessagingException {
        String link = articleId != null ? "https://www.shisanyue.site/blog/article/"+articleId : "https://www.shisanyue.site/message";
        Map<String, Object> valueMap = new HashMap<>();
        valueMap.put("from", email);
        valueMap.put("to", toemail);
        valueMap.put("title", "你收到一条回复(十三月的博客)");
        valueMap.put("link", link);
        valueMap.put("qqName", qqName);
        valueMap.put("qqImage", qqImage);
        valueMap.put("content", content);
        emailTool.sendSimpleMail(valueMap);
        return "success";
    }
}
