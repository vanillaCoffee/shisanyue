package com.ssy.controller.site;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.ContentDomain;
import com.ssy.dto.MetaDto;
import com.ssy.dto.cond.ContentCond;
import com.ssy.constant.Types;
import com.ssy.constant.WebConst;
import com.ssy.model.ArchiveVO;
import com.ssy.model.UserConnectionVO;
import com.ssy.service.content.ContentService;
import com.ssy.service.meta.MetaService;
import com.ssy.service.site.SiteService;
import com.ssy.service.user.UserService;
import com.ssy.utils.ArticleUtil;
import com.ssy.utils.DateUtil;
import com.ssy.utils.SiteUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;

@Controller
public class ArchiveController {

    @Autowired
    private UserService userService;

    @Autowired
    private ContentService contentService;

    @Autowired
    private MetaService metaService;

    @Autowired
    private SiteService siteService;

    @Autowired
    private SiteUtil siteUtil;

    @Autowired
    private DateUtil date;

    @Autowired
    private ArticleUtil articleUtil;

    @Autowired
    private ProviderSignInUtils providerSignInUtils;

    @GetMapping(value = "/blog/{category}/{page}")
    public String categories(
            @PathVariable("category")
                    String category,
            @PathVariable("page")
                    int page,
            @RequestParam(name = "limit", required = false, defaultValue = "1000")
                    int limit,
            HttpServletRequest request
    ) {
        ContentCond contentCond = new ContentCond();
        ContentCond contentCond1 = new ContentCond();
        contentCond.setType(Types.ARTICLE.getType());
        contentCond.setCategory(category);
        contentCond1.setType(Types.ARTICLE.getType());
        List<MetaDto> categories = metaService.getMetaListByMap(Types.CATEGORY.getType(), null, WebConst.MAX_POSTS);
        List<ContentDomain> articleList = contentService.getArticleListByCond(contentCond1);
        List<ContentDomain> hotArticles = articleList.subList(0,6);
        PageInfo<ContentDomain> articles = contentService.getArticleByCond(contentCond, page, limit);
        Collections.reverse(articleList);
        List<UserConnectionVO> userConnectionVOList = userService.getUserHeadOrderByTime();
//        UserConnectionVO thirdUserInfoRecently = userService.getThirdUserInfoRecently();
        Connection connection = providerSignInUtils.getConnectionFromSession(new ServletWebRequest(request));
        UserConnectionVO userConnectionVO = new UserConnectionVO();
        if (connection != null) {
//            log.info("第三方用户信息"+connection.toString());

            userConnectionVO.setDisplayName(connection.getDisplayName());
            userConnectionVO.setImageUrl(connection.getImageUrl());
        }
        request.setAttribute("articles", articles);
        request.setAttribute("date", date);
        request.setAttribute("site", siteUtil);
        request.setAttribute("categories", categories);
        request.setAttribute("articleList", articleList);
        request.setAttribute("hotArticles", hotArticles);
        request.setAttribute("articleUtil", articleUtil);
        request.setAttribute("userInfo", userConnectionVOList);
        request.setAttribute("thirdUserInfoRecently", userConnectionVO);

        return "site/blog";
    }

    @GetMapping(value = {"/archives", "/archives/index"})
    public String archives(HttpServletRequest request) {
        ContentCond contentCond = new ContentCond();
        contentCond.setType(Types.ARTICLE.getType());
        List<ArchiveVO> archives = siteService.getArchives(contentCond);
        Collections.reverse(archives);
        request.setAttribute("archives_list", archives);
        request.setAttribute("site", siteUtil);
        return "site/archive";
    }
}
