package com.ssy.controller.site;

import com.github.pagehelper.PageInfo;
import com.ssy.dao.UserDao;
import com.ssy.domain.CommentDomain;
import com.ssy.domain.ContentDomain;
import com.ssy.domain.UserConnectionDomain;
import com.ssy.dto.CommentDto;
import com.ssy.dto.MetaDto;
import com.ssy.dto.UserDto;
import com.ssy.dto.cond.CommentCond;
import com.ssy.dto.cond.ContentCond;
import com.ssy.config.security.MyUserDetails;
import com.ssy.constant.Types;
import com.ssy.constant.WebConst;
import com.ssy.model.ContentVO;
import com.ssy.model.UserConnectionVO;
import com.ssy.oauth2.QQApiImpl;
import com.ssy.service.comment.CommentService;
import com.ssy.service.content.ContentService;
import com.ssy.service.meta.MetaService;
import com.ssy.service.user.UserService;
import com.ssy.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import java.util.Collections;
import java.util.List;

@Controller
@Slf4j
public class BlogController {

    @Autowired
    private UserDao userDao;

    @Autowired
    private ContentService contentService;

    @Autowired
    private MetaService metaService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private SiteUtil siteUtil;

    @Autowired
    private DateUtil date;

    @Autowired
    private DateKit dateKit;

    @Autowired
    private MarkdownUtil markdownUtil;

    @Autowired
    private StringUtil stringUtil;

    @Autowired
    private ArticleUtil articleUtil;

    @Autowired
    private UserUtil userUtil;

    @Autowired
    private UserService userService;


    @GetMapping(value = {"/index", "/"})
    public String getBlogIndex(
            HttpServletRequest request,
            @RequestParam(name = "page", required = false, defaultValue = "1")
                    int page,
            @RequestParam(name = "limit", required = false, defaultValue = "3")
                    int limit) {
        HttpSession session = request.getSession();
        UserConnectionDomain userConnectionDomain = userService.getProviderByProviderUserId(QQApiImpl.allOpenId);
        if (userConnectionDomain != null) {
            session.setAttribute("qqImage", userConnectionDomain.getImageUrl());
            session.setAttribute("qqName", userConnectionDomain.getDisplayName());
            session.setAttribute("qqOpenId", userConnectionDomain.getProviderUserId());
            session.setAttribute("email", userConnectionDomain.getEmail());
        } else {
            session.setAttribute("email", null);
        }
        QQApiImpl.allOpenId = null;
        PageInfo<ContentDomain> articles = contentService.getArticleByCond(new ContentCond(), page, limit);
        Collections.reverse(articles.getList());
        request.setAttribute("date", date);
        request.setAttribute("site", siteUtil);
        request.setAttribute("articles", articles);
        return "site/index";
    }

    @GetMapping(value = "/blog")
    public String getBlog(
            HttpServletRequest request,
//            @PathVariable("p")
            @RequestParam(name = "p", required = false, defaultValue = "1")
                    int p,
            @RequestParam(name = "limit", required = false, defaultValue = "9999")
                    int limit,
            HttpSession session) {
        p = p < 0 || p > WebConst.MAX_PAGE ? 1 : p;
        ContentCond contentCond = new ContentCond();
        contentCond.setType(Types.ARTICLE.getType());
        PageInfo<ContentDomain> articles = contentService.getArticleByCond(contentCond, p, limit);
        List<ContentDomain> articleList = contentService.getArticleListByCond(contentCond);
        if (articleList.size() > 7) {
            List<ContentDomain> hotArticles = articleList.subList(0, 6);
            request.setAttribute("hotArticles", hotArticles);
        }
        List<MetaDto> categories = metaService.getMetaListByMap(Types.CATEGORY.getType(), null, WebConst.MAX_POSTS);
        List<UserConnectionVO> userConnectionVOList = userService.getUserHeadOrderByTime();
        Collections.reverse(articles.getList());
        Collections.reverse(articleList);

        request.setAttribute("categories", categories);
        request.setAttribute("length", categories.size());
        request.setAttribute("date", date);
        request.setAttribute("site", siteUtil);
        request.setAttribute("articles", articles);
        request.setAttribute("articleUtil", articleUtil);
        request.setAttribute("articleList", articleList);
        request.setAttribute("userInfo", userConnectionVOList);

        return "site/blog";
    }

    @GetMapping(value = {"/blog/article/{cid}", "/work/article/{cid}"})
    public String getBlogDetail(
            @PathVariable("cid")
                    Integer cid,
            HttpServletRequest request
    ) {
        if (request.getUserPrincipal() != null) {
            MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            UserDto userDto = userDao.getUserInfoByUsernameAndSysUser(myUserDetails.getUsername());
            request.setAttribute("user", userDto);
        }
        CommentCond commentCond = new CommentCond();
        commentCond.setType(Types.COMMENT.getType());
        commentCond.setContentId(cid);
        commentCond.setParent(0);

        //获取文章
        ContentDomain article = contentService.getArticleByContentId(cid);
        //获取所有的评论
        List<CommentDto> allComments = commentService.getComments(commentCond);
        Collections.reverse(allComments);
        //更新点击数
        int hits = article.getHits() + 1;
        article.setHits(hits);
        contentService.updateArticleByContentId(article);
        ContentCond contentCond = new ContentCond();
        contentCond.setType(Types.ARTICLE.getType());
        List<CommentDomain> commentsPaginator = commentService.getCommentsByContentId(cid);
        //根据文章号获取前一篇文章和后一篇文章
        List<ContentVO> BeforeAndAfterArticleList = contentService.getBeforeAndAfterArticle(cid);
        request.setAttribute("date", date);
        request.setAttribute("article", article);
        request.setAttribute("dateKit", dateKit);
        request.setAttribute("comments", commentsPaginator);
        request.setAttribute("BeforeAndAfterArticleList", BeforeAndAfterArticleList);
        request.setAttribute("site", siteUtil);
        request.setAttribute("markdown", markdownUtil);
        request.setAttribute("active", "blog");
        request.setAttribute("stringUtil", stringUtil);
        request.setAttribute("articleUtil", articleUtil);
        request.setAttribute("commentList", allComments);
        request.setAttribute("userUtil", userUtil);
        return "site/blog_detail";
    }
}
