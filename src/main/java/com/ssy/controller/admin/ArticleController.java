package com.ssy.controller.admin;

import com.github.pagehelper.PageInfo;
import com.ssy.dao.UserDao;
import com.ssy.domain.ContentDomain;
import com.ssy.domain.MetaDomain;
import com.ssy.dto.UserDto;
import com.ssy.dto.cond.ContentCond;
import com.ssy.dto.cond.MetaCond;
import com.ssy.config.security.MyUserDetails;
import com.ssy.constant.LogActions;
import com.ssy.constant.Types;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.content.ContentService;
import com.ssy.service.log.LogService;
import com.ssy.service.meta.MetaService;
import com.ssy.utils.ArticleUtil;
import com.ssy.utils.DateKit;
import com.ssy.utils.RandomUtil;
import com.ssy.utils.SiteUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
@RequestMapping("/admin/article")
@Slf4j
public class ArticleController {

    @Autowired
    private ContentService contentService;

    @Autowired
    private MetaService metaService;

    @Autowired
    private LogService logService;

    @Autowired
    private RandomUtil random;

    @Autowired
    private UserDao userDao;

    @Autowired
    private DateKit dateFormat;

    @Autowired
    private SiteUtil siteUtil;

    @Autowired
    private ArticleUtil articleUtil;

    @GetMapping(value = "")
    public String index(
            HttpServletRequest request,
            @RequestParam(name = "page", required = false, defaultValue = "1")
                    int page,
            @RequestParam(name = "limit", required = false, defaultValue = "10")
                    int limit
    ) {
        PageInfo<ContentDomain> articles = contentService.getArticleByCond(new ContentCond(), page, limit);
        request.setAttribute("date", dateFormat);
        request.setAttribute("site", siteUtil);
        request.setAttribute("articles", articles);
        return "admin/article/manage/article_list";
    }

    @GetMapping(value = "/publish")
    public String newArticle(HttpServletRequest request) {
        MetaCond metaCond = new MetaCond();
        metaCond.setMetaType(Types.CATEGORY.getType());
        List<MetaDomain> metas = metaService.getMetasByCond(metaCond);
        request.setAttribute("categories", metas);
        request.setAttribute("articleUtil", articleUtil);
        return "admin/article/publish/article_edit";
    }

    @PostMapping(value = "/publish")
    @ResponseBody
    public LayUiJsonResponse publishArticle(
            @RequestParam(name = "title", required = true)
                    String title,
            @RequestParam(name = "credentialOne", required = false)
                    String titlePic,
            @RequestParam(name = "music", required = false)
                    String music,
            @RequestParam(name = "source", required = true)
                    String source,
            @RequestParam(name = "slug", required = false)
                    String slug,
            @RequestParam(name = "content", required = true)
                    String content,
            @RequestParam(name = "type", required = true)
                    String type,
            @RequestParam(name = "status", required = true)
                    String status,
            @RequestParam(name = "tags", required = false)
                    String tags,
            @RequestParam(name = "categories", required = false, defaultValue = "默认分类")
                    String categories,
            @RequestParam(name = "allowComment", required = false)
                    Integer allowComment,
            @RequestParam(name = "top", required = false, defaultValue = "0")
                    Integer top
    ) {
        ContentDomain contentDomain = new ContentDomain();
        contentDomain.setTitle(title);
        contentDomain.setTitlePic(titlePic);
        contentDomain.setMusic(music);
        contentDomain.setTop(top);
        contentDomain.setSource(source);
        contentDomain.setSlug(slug);
        contentDomain.setContent(content);
        contentDomain.setType(type);
        contentDomain.setStatus(status);
        contentDomain.setTags(type.equals(Types.ARTICLE.getType()) ? tags : null);
        contentDomain.setHits(0);
        contentDomain.setCommentsNum(0);
        //只允许博客文章有分类，防止作品被收入分类
        contentDomain.setCategories(type.equals(Types.ARTICLE.getType()) ? categories : null);
        contentDomain.setAllowComment(allowComment);
        contentService.addArticle(contentDomain);
        return LayUiJsonResponse.success();
    }

    @PutMapping(value = "/{cid}/{top}")
    @ResponseBody
    public LayUiJsonResponse topArticle(@PathVariable Integer cid, @PathVariable Integer top, HttpServletRequest request) {
        ContentDomain contentCond = new ContentDomain();
        contentCond.setTop(top);
        contentCond.setContentId(cid);
        int res = contentService.updateArticleByContentId(contentCond);
        if (res > 0 & top == 1) {
            return LayUiJsonResponse.success("文章已置顶");
        } else if (res > 0 & top == 0) {
            return LayUiJsonResponse.success("文章已取消置顶");
        } else {
            return LayUiJsonResponse.fail("系统错误");
        }
    }


    @GetMapping(value = "/{cid}")
    public String editArticle(
            @PathVariable
                    Integer cid,
            HttpServletRequest request
    ) {
        ContentDomain content = contentService.getArticleByContentId(cid);
        request.setAttribute("contents", content);
        MetaCond metaCond = new MetaCond();
        metaCond.setMetaType(Types.CATEGORY.getType());
        List<MetaDomain> categories = metaService.getMetasByCond(metaCond);
        request.setAttribute("categories", categories);
        request.setAttribute("active", "article");
        request.setAttribute("articleUtil", articleUtil);
        return "admin/article/publish/article_edit";
    }


    @PostMapping("/modify")
    @ResponseBody
    public LayUiJsonResponse modifyArticle(
            @RequestParam(name = "cid", required = true)
                    Integer cid,
            @RequestParam(name = "title", required = true)
                    String title,
            @RequestParam(name = "source", required = true)
                    String source,
            @RequestParam(name = "credentialOne", required = false)
                    String titlePic,
            @RequestParam(name = "music", required = false)
                    String music,
            @RequestParam(name = "slug", required = false)
                    String slug,
            @RequestParam(name = "content", required = true)
                    String content,
            @RequestParam(name = "type", required = true)
                    String type,
            @RequestParam(name = "status", required = true)
                    String status,
            @RequestParam(name = "tags", required = false)
                    String tags,
            @RequestParam(name = "categories", required = false, defaultValue = "默认分类")
                    String categories,
            @RequestParam(name = "allowComment", required = false)
                    Integer allowComment,
            @RequestParam(name = "top", required = false, defaultValue = "0")
                    Integer top
    ) {
        ContentDomain contentDomain = new ContentDomain();
        contentDomain.setContentId(cid);
        contentDomain.setTop(top);
        contentDomain.setTitle(title);
        contentDomain.setTitlePic(titlePic);
        contentDomain.setMusic(music);
        contentDomain.setSource(source);
        contentDomain.setSlug(slug);
        contentDomain.setContent(content);
        contentDomain.setType(type);
        contentDomain.setStatus(status);
        contentDomain.setTags(tags);
        contentDomain.setAllowComment(allowComment);
        //只允许博客文章有分类，防止作品被收入分类
        contentDomain.setCategories(type.equals(Types.ARTICLE.getType()) ? categories : null);
        contentService.updateArticleByContentId(contentDomain);
        return LayUiJsonResponse.success();
    }


    @PostMapping(value = "/delete")
    @ResponseBody
    public LayUiJsonResponse deleteArticle(
            @RequestParam(name = "cid", required = true)
                    Integer cid
    ) {
        MyUserDetails userDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDto userDto = userDao.getUserInfoByUsernameAndSysUser(userDetails.getUsername());
        contentService.delArticleByContentId(cid);
        logService.addLog(LogActions.DEL_ARTICLE.getAction(), cid + "", ((WebAuthenticationDetails) SecurityContextHolder.getContext().
                getAuthentication().getDetails()).getRemoteAddress(), userDto.getUserId());
        return LayUiJsonResponse.success();
    }
}
