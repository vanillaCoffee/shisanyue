package com.ssy.controller.admin;

import com.ssy.dto.MetaDto;
import com.ssy.dto.cond.MetaCond;
import com.ssy.constant.Types;
import com.ssy.constant.WebConst;
import com.ssy.exception.BusinessException;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.meta.MetaService;
import com.ssy.utils.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("admin/category")
public class CategoryController {
    @Autowired
    private MetaService metaService;

    @Autowired
    private RandomUtil random;

    @GetMapping(value = "")
    public String index(HttpServletRequest request) {
        List<MetaDto> categories = metaService.getMetaListByMap(Types.CATEGORY.getType(), null, WebConst.MAX_POSTS);
        List<MetaDto> tags = metaService.getMetaListByMap(Types.TAG.getType(), null, WebConst.MAX_POSTS);
        request.setAttribute("categories", categories);
        request.setAttribute("tags", tags);
        request.setAttribute("random", random);
        return "admin/category/category";
    }

    @PostMapping(value = "/add")
    @ResponseBody
    public LayUiJsonResponse addCategory(
            @RequestParam(name = "cname", required = true)
                    String cname
    ) {
        try {
            metaService.addMeta(Types.CATEGORY.getType(), cname);

        } catch (Exception e) {
            e.printStackTrace();
            String msg = "分类新增失败";
            if (e instanceof BusinessException) {
                BusinessException ex = (BusinessException) e;
                msg = ex.getMessage();
            }

            return LayUiJsonResponse.fail(msg);
        }
        return LayUiJsonResponse.success("新增分类成功");
    }

    @PostMapping(value = "/delete")
    @ResponseBody
    public LayUiJsonResponse delete(
            @RequestParam(name = "mid", required = true)
                    Integer mid
    ) {
        try {
            metaService.delMetaByMetaId(mid);

        } catch (Exception e) {
            e.printStackTrace();
            return LayUiJsonResponse.fail(e.getMessage());
        }
        return LayUiJsonResponse.success("删除分类成功");
    }

    @PostMapping(value = "/update")
    @ResponseBody
    public LayUiJsonResponse updateCategory(
            @RequestParam(name = "cname", required = true)
                    String cname,
            @RequestParam(name = "mid", required = false)
                    Integer mid
    ) {
        try {
            MetaCond metaCond = new MetaCond();
            metaCond.setMetaType(Types.CATEGORY.getType());
            metaCond.setMetaName(cname);
            metaCond.setMetaId(mid);
            metaService.updateMetaName(metaCond);

        } catch (Exception e) {
            e.printStackTrace();
            String msg = "更新分类失败";
            if (e instanceof BusinessException) {
                BusinessException ex = (BusinessException) e;
                msg = ex.getMessage();
            }

            return LayUiJsonResponse.fail(msg);
        }
        return LayUiJsonResponse.success("更新分类成功");
    }
}
