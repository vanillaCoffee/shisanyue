package com.ssy.controller.admin;

import com.ssy.domain.MetaDomain;
import com.ssy.dto.cond.MetaCond;
import com.ssy.constant.ExceptionEnum;
import com.ssy.constant.Types;
import com.ssy.exception.BusinessException;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.meta.MetaService;
import com.ssy.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping(value = "admin/links")
public class LinksController {
    @Autowired
    private MetaService metaService;

    @Autowired
    private StringUtil stringUtil;

    @GetMapping(value = "")
    public String index(HttpServletRequest request) {

        MetaCond metaCond = new MetaCond();
        metaCond.setMetaType(Types.LINK.getType());
        List<MetaDomain> metas = metaService.getMetasByCond(metaCond);
        request.setAttribute("links", metas);
        request.setAttribute("stringUtil", stringUtil);
        return "admin/links/links";
    }

    @PostMapping(value = "add")
    @ResponseBody
    public LayUiJsonResponse addLink(
            @RequestParam(name = "title", required = true)
                    String title,
            @RequestParam(name = "url", required = true)
                    String url,
            @RequestParam(name = "logo", required = false)
                    String logo,
            @RequestParam(name = "mid", required = false)
                    Integer mid,
            @RequestParam(name = "description", required = false)
                    String description,
            @RequestParam(name = "sort", required = false, defaultValue = "0")
                    int sort
    ){
        try {
            MetaDomain meta = new MetaDomain();
            meta.setMetaName(title);
            meta.setSlug(url);
            meta.setIcon(logo);
            meta.setSort(sort);
            meta.setDescription(description);
            meta.setMetaType(Types.LINK.getType());
            if (null != mid){
                meta.setMetaId(mid);
                metaService.updateMeta(meta);
            }else {
                metaService.addMeta(meta);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ExceptionEnum.ADD_META_FAIL);
        }
        return LayUiJsonResponse.success("添加友链成功");
    }

    @PostMapping(value = "delete")
    @ResponseBody
    public LayUiJsonResponse delete(
            @RequestParam(name = "mid", required = true)
                    int mid
    ) {
        try {
            metaService.delMetaByMetaId(mid);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(ExceptionEnum.DELETE_META_FAIL);
        }
        return LayUiJsonResponse.success();

    }
}
