package com.ssy.controller.admin;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.TimeDomain;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.timeline.TimeLineService;
import com.ssy.utils.DateKit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/admin/timeline")
public class TimeLineController {
    @Autowired
    private TimeLineService timeLineService;

    @Autowired
    private DateKit dateFormat;

    @GetMapping("/list")
    public String timeline(HttpServletRequest request,
                           @RequestParam(name = "page", required = false, defaultValue = "1")
                                   int page,
                           @RequestParam(name = "limit", required = false, defaultValue = "10")
                                   int limit) {
        PageInfo<TimeDomain> timeDomainList = timeLineService.getTimeLine(page, limit);
        request.setAttribute("timeline", timeDomainList);
        request.setAttribute("date", dateFormat);
        return "admin/timeline/manage/timeline_list";
    }

    @GetMapping("/edit")
    public String editTimeline() {
        return "admin/timeline/publish/timeline_edit";
    }

    @GetMapping(value = "/{tid}")
    public String TimelineEdit(
            @PathVariable Integer tid,
            HttpServletRequest request
    ) {
        TimeDomain timeDomain = timeLineService.getTimeLineByTimeId(tid);
        request.setAttribute("timeline", timeDomain);
        return "admin/timeline/publish/timeline_edit";
    }

    @PostMapping("/add")
    @ResponseBody
    public LayUiJsonResponse newTimeline(
            @RequestParam(name = "content", required = true)
                    String content,
            @RequestParam(name = "status", required = true)
                    String status
    ) {
        TimeDomain timeDomain = new TimeDomain();
        timeDomain.setContent(content);
        timeDomain.setStatus(status);
        int res = timeLineService.addTimeLine(timeDomain);
        if (res > 0) {
            return LayUiJsonResponse.success("时光沙漏制作成功!");
        } else {
            return LayUiJsonResponse.fail("时光沙漏制作失败!");
        }
    }

    @PostMapping("/edit")
    @ResponseBody
    public LayUiJsonResponse TimelineEdit(
            @RequestParam(name = "tid", required = true)
                    Integer tid,
            @RequestParam(name = "content", required = true)
                    String content,
            @RequestParam(name = "status", required = true)
                    String status
    ) {
        TimeDomain timeDomain = new TimeDomain();
        timeDomain.setTimeId(tid);
        timeDomain.setContent(content);
        timeDomain.setStatus(status);
        int res = timeLineService.updateTimeLine(timeDomain);
        if (res > 0) {
            return LayUiJsonResponse.success("时光沙漏更新成功!");
        } else {
            return LayUiJsonResponse.fail("时光沙漏更新失败!");
        }
    }

    @DeleteMapping("")
    @ResponseBody
    public LayUiJsonResponse delTimeline(Integer timeId) {
        int res = timeLineService.delTimeLine(timeId);
        if (res > 0) {
            return LayUiJsonResponse.success("删除成功");
        } else {
            return LayUiJsonResponse.fail("删除失败");
        }
    }
}
