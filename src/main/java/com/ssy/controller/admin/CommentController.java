package com.ssy.controller.admin;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.CommentDomain;
import com.ssy.dto.CommentDto;
import com.ssy.dto.cond.CommentCond;
import com.ssy.constant.ExceptionEnum;
import com.ssy.exception.BusinessException;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.comment.CommentService;
import com.ssy.utils.DateKit;
import com.ssy.utils.MarkdownUtil;
import com.ssy.utils.SiteUtil;
import com.ssy.utils.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

@Controller
@RequestMapping("/admin/comments")
public class CommentController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private DateKit dateFormat;

    @Autowired
    private SiteUtil siteUtil;

    @Autowired
    private MarkdownUtil markdownUtil;

    @Autowired
    private StringUtil stringUtil;

    @GetMapping(value = "")
    public String index(
            @RequestParam(name = "page", required = false, defaultValue = "1")
                    int page,
            @RequestParam(name = "limit", required = false, defaultValue = "15")
                    int limit,
            HttpServletRequest request
    ) {

        PageInfo<CommentDto> comments = commentService.getCommentsByCond(new CommentCond(), page, limit);
        Collections.reverse(comments.getList());
        request.setAttribute("comments", comments);
        request.setAttribute("site", siteUtil);
        request.setAttribute("date",dateFormat);
        request.setAttribute("markdown",markdownUtil);
        request.setAttribute("stringUtil", stringUtil);
        return "admin/comment/comment";
    }

    @PostMapping(value = "/delete")
    @ResponseBody
    public LayUiJsonResponse deleteComment(
            @RequestParam(name = "coid", required = true)
                    Integer coid
    ) {
        try {
            CommentDomain comment = commentService.getCommentsByCommentId(coid);
            if (null == comment)
                throw new BusinessException(ExceptionEnum.DELETE_COMMENT_FAIL);
            commentService.delComment(coid);
        } catch (Exception e) {
            e.printStackTrace();
            return LayUiJsonResponse.fail(e.getMessage());
        }
        return LayUiJsonResponse.success();
    }

    @PostMapping(value = "/status")
    @ResponseBody
    public LayUiJsonResponse changeStatus(
            @RequestParam(name = "coid", required = true)
                    Integer coid,
            @RequestParam(name = "status", required = true)
                    String status
    ) {
        try {
            CommentDomain comment = commentService.getCommentsByCommentId(coid);
            if (null != comment) {
                commentService.updateCommentStatus(coid, status);
            } else {
                return LayUiJsonResponse.fail("删除失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return LayUiJsonResponse.fail(e.getMessage());
        }
        return LayUiJsonResponse.success();
    }
}
