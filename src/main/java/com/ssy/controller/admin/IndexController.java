package com.ssy.controller.admin;

import com.github.pagehelper.PageInfo;
import com.ssy.domain.ContentDomain;
import com.ssy.domain.LogDomain;
import com.ssy.domain.UserConnectionDomain;
import com.ssy.dto.CommentDto;
import com.ssy.dto.StatisticsDto;
import com.ssy.dto.cond.UserCond;
import com.ssy.config.security.MyUserDetailService;
import com.ssy.config.security.MyUserDetails;
import com.ssy.exception.BusinessException;
import com.ssy.model.MessageVO;
import com.ssy.model.UserVO;
import com.ssy.oauth2.QQApiImpl;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.log.LogService;
import com.ssy.service.site.SiteService;
import com.ssy.service.user.UserService;
import com.ssy.utils.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping(value = "/admin")
public class IndexController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IndexController.class);
    @Autowired
    private SiteService siteService;

    @Autowired
    private LogService logService;

    @Autowired
    private UserService userService;

    @Autowired
    private MyUserDetailService myUserDetailService;

    @Autowired
    private DateKit dateFormat;

    @Autowired
    private SiteUtil siteUtil;

    @Autowired
    private MarkdownUtil markdownUtil;

    @Autowired
    private ArticleUtil articleUtil;


    @GetMapping(value = "/index")
    public String index(HttpServletRequest request) {
        List<CommentDto> comments = siteService.getComments(5);
        List<MessageVO> messages = siteService.getRecentlyMessage(5);
        List<ContentDomain> contents = siteService.getNewArticles(5);
        StatisticsDto statistics = siteService.getStatistics();
        PageInfo<LogDomain> logs = logService.getLogs(1, 5);
        List<LogDomain> logsList = logs.getList();
        Collections.reverse(comments);
        HttpSession session = request.getSession();
        UserConnectionDomain userConnectionDomain = userService.getProviderByProviderUserId("4DD96C26A90E704BF60CF85CA4ABF063");
        if (userConnectionDomain != null) {
            session.setAttribute("qqImage", userConnectionDomain.getImageUrl());
            session.setAttribute("qqName", userConnectionDomain.getDisplayName());
            session.setAttribute("email", userConnectionDomain.getEmail());
        }
        request.setAttribute("comments", comments);
        request.setAttribute("messages", messages);
        request.setAttribute("articles", contents);
        request.setAttribute("statistics", statistics);
        request.setAttribute("logs", logsList);
        request.setAttribute("site", siteUtil);
        request.setAttribute("date", dateFormat);
        request.setAttribute("markdown", markdownUtil);
        request.setAttribute("articleUtil", articleUtil);

        return "admin/index/index";
    }

    /**
     * 个人设置页面
     */
    @GetMapping(value = "/profile")
    public String profile(HttpServletRequest request) {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserVO userVo = userService.getUserVoByUsername(myUserDetails.getUsername());
        request.setAttribute("userInfo", userVo);
        return "admin/setting/profile";
    }

    /**
     * 保存个人信息
     */
    @PostMapping(value = "/profile")
    @ResponseBody
    public LayUiJsonResponse saveProfile(String screenName, String email) {
        MyUserDetails myUserDetails = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (StringUtils.isNotBlank(screenName) && StringUtils.isNotBlank(email)) {
            UserCond userCond = new UserCond();
            userCond.setScreenName(screenName);
            userCond.setEmail(email);
            userCond.setUsername(myUserDetails.getUsername());
            userService.updateUserInfoByUserCond(userCond);
        }
        return LayUiJsonResponse.success();
    }

    /**
     * 修改密码
     */
    @PostMapping(value = "/password")
    @ResponseBody
    public LayUiJsonResponse upPwd(String password, String newPassword, String newPasswordAgain) {
        SecurityContext context = SecurityContextHolder.getContext();
        Authentication authentication = context.getAuthentication();
        String username = authentication.getName();
        //根据用户名获得用户
        MyUserDetails user = userService.getUserInfoByUsername(username);
        UserCond userCond = new UserCond();
        String databasePassword = user.getPassword();
        BCryptPasswordEncoder bc = new BCryptPasswordEncoder();
        boolean matches = bc.matches(password, databasePassword);
        if (StringUtils.isBlank(password) || StringUtils.isBlank(newPassword)) {
            return LayUiJsonResponse.fail("请确认信息输入完整");
        }
        if (!matches) {
            return LayUiJsonResponse.fail("旧密码错误");
        }
        if (newPassword.length() < 6 || newPassword.length() > 14) {
            return LayUiJsonResponse.fail("请输入6-14位密码");
        }
        if (!newPassword.equals(newPasswordAgain)) {
            return LayUiJsonResponse.fail("两次密码不一致");
        }
        try {
            userCond.setPassword(BCryptUtil.encode(newPassword));
            userCond.setUsername(username);
            userService.updateUserInfoByUserCond(userCond);
            return LayUiJsonResponse.success();
        } catch (Exception e) {
            String msg = "密码修改失败";
            if (e instanceof BusinessException) {
                msg = e.getMessage();
            } else {
                LOGGER.error(msg, e);
            }
            return LayUiJsonResponse.fail(msg);
        }
    }

}
