package com.ssy.controller.admin;

import com.github.pagehelper.PageInfo;
import com.ssy.api.QiniuCloudService;
import com.ssy.domain.AttAchDomain;
import com.ssy.dto.AttAchDto;
import com.ssy.dto.UserDto;
import com.ssy.config.security.MyUserDetailService;
import com.ssy.config.security.MyUserDetails;
import com.ssy.constant.ExceptionEnum;
import com.ssy.constant.Types;
import com.ssy.constant.WebConst;
import com.ssy.exception.BusinessException;
import com.ssy.model.AttAchesVO;
import com.ssy.response.LayUiJsonResponse;
import com.ssy.service.attach.AttAchService;
import com.ssy.service.user.UserService;
import com.ssy.utils.AttAchUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping("admin/attach")
public class AttAchController {

    @Autowired
    private AttAchService attAchService;

    @Autowired
    private QiniuCloudService qiniuCloudService;

    @Autowired
    private MyUserDetailService myUserDetailService;

    @Autowired
    private UserService userService;

    @GetMapping(value = "")
    public String index(
            @RequestParam(name = "page", required = false, defaultValue = "1")
                    int page,
            @RequestParam(name = "limit", required = false, defaultValue = "12")
                    int limit,
            HttpServletRequest request
    ) {
        PageInfo<AttAchDto> attaches = attAchService.getAttAches(page, limit);
        PageInfo<AttAchesVO> attachesVO = attAchService.getAttAchesByFileType("image", page, limit);
        request.setAttribute("attaches", attaches);
        request.setAttribute("attachesVO", attachesVO);
        request.setAttribute("max_file_size", WebConst.MAX_FILE_SIZE / 1024);
        return "admin/attach/attach";
    }

    @PostMapping(value = "upload")
    @ResponseBody
    public LayUiJsonResponse filesUploadToCloud(HttpServletRequest request,
                                                HttpServletResponse response,
                                                @RequestParam(name = "file", required = true)
                                                        MultipartFile[] files) {
        MyUserDetails user = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDto temp = userService.getUserInfoByUsernameAndSysUser(user.getUsername());
        //文件上传
        try {
            List<String> picUrl = new ArrayList<>();
            request.setCharacterEncoding("utf-8");
            response.setHeader("Content-Type", "text/html");

            for (MultipartFile file : files) {

                String fileName = AttAchUtil.getFileName(file.getOriginalFilename()).replaceFirst("/", "");
                picUrl.add(qiniuCloudService.QINIU_UPLOAD_SITE + "/" + fileName);
                qiniuCloudService.upload(file, fileName);
                AttAchDomain attAch = new AttAchDomain();
                attAch.setAuthorId(temp.getUserId());
                attAch.setFileType(AttAchUtil.isImage(file.getInputStream()) ? Types.IMAGE.getType() : Types.FILE.getType());
                attAch.setFileName(fileName);
                attAch.setFileKey(qiniuCloudService.QINIU_UPLOAD_SITE + "/" + fileName);
                attAchService.addAttach(attAch);
            }
//            System.out.println(picUrl.get(0));
            Map<String, String> map = new HashMap<>();
            map.put("src", picUrl.get(0));
            List<Map<String, String>> list = new ArrayList<>();
            list.add(map);
            return LayUiJsonResponse.success("附件上传成功", list);
        } catch (IOException e) {
            e.printStackTrace();
            throw new BusinessException(ExceptionEnum.ADD_NEW_ATT_FAIL);
        }
    }


    @PostMapping(value = "/uploadfile")
    @ResponseBody
    public  Map<String, Object> demo(@RequestParam(value = "editormd-image-file", required = false) MultipartFile file, HttpServletRequest request) {
        MyUserDetails user = (MyUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDto temp = userService.getUserInfoByUsernameAndSysUser(user.getUsername());
        Map<String, Object> resultMap = new HashMap<String, Object>();
        //保存
        try {
            String fileName = AttAchUtil.getFileName(file.getOriginalFilename()).replaceFirst("/", "");
            qiniuCloudService.upload(file, fileName);
            AttAchDomain attAch = new AttAchDomain();
            attAch.setAuthorId(temp.getUserId());
            attAch.setFileType("image");
            attAch.setFileName(fileName);
            attAch.setFileKey(qiniuCloudService.QINIU_UPLOAD_SITE + "/" + fileName);
            attAchService.addAttach(attAch);

            resultMap.put("success", 1);
            resultMap.put("message", "上传成功！");
            resultMap.put("url", "https://" + qiniuCloudService.QINIU_UPLOAD_SITE + "/" + fileName);

        } catch (Exception e) {
            resultMap.put("success", 0);
            resultMap.put("message", "上传失败！");
            e.printStackTrace();
        }
        return resultMap;
    }


    @DeleteMapping("")
    @ResponseBody
    public LayUiJsonResponse delAttache(@RequestParam(name = "attacheId", required = true) int attacheId) {
        int res = attAchService.delAttAch(attacheId);
        if (res > 0) {
            return LayUiJsonResponse.success("删除成功");
        } else {
            return LayUiJsonResponse.fail("删除成功");
        }
    }
}
