package com.ssy.controller.admin;

import com.ssy.domain.OptionDomain;
import com.ssy.service.log.LogService;
import com.ssy.service.option.OptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/admin/setting")
public class SettingController {
    @Autowired
    private OptionService optionService;

    @Autowired
    private LogService logService;



    @GetMapping(value = "")
    public String setting(HttpServletRequest request) {
        List<OptionDomain> optionsList = optionService.getOptions();
        Map<String, String> options = new HashMap<>();
        optionsList.forEach((option) -> {
            options.put(option.getName(), option.getValue());
        });
        request.setAttribute("options", options);
        return "admin/setting/setting";
    }

//    @PostMapping(value = "")
//    @ResponseBody
//    public LayUiJsonResponse saveSetting(HttpServletRequest request) {
//        try {
//            UserDto userDto = (UserDto) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            Map<String, String[]> parameterMap = request.getParameterMap();
//            Map<String, String> querys = new HashMap<>();
//            parameterMap.forEach((key, value) -> {
//                querys.put(key, StringUtil.join(value));
//            });
//            optionService.(querys);
//            WebConst.initConfig = querys;
//
//            logService.addLog(LogActions.SYS_SETTING.getAction(), FastJsonUtil.toJSONString(querys), request.getRemoteAddr(), userDto.getUserId());
//            return LayUiJsonResponse.success();
//        } catch (Exception e) {
//            String msg = "保存设置失败";
//            return LayUiJsonResponse.fail(e.getMessage());
//        }
//    }
}
