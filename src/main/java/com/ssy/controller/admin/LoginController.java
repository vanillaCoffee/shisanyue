package com.ssy.controller.admin;

import com.ssy.utils.RandomUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;



import javax.servlet.http.HttpServletRequest;

@Controller
public class LoginController {
    @Autowired
    private RandomUtil random;

    @GetMapping ("/toLogin")
    public String toLogin(HttpServletRequest request) {
        request.setAttribute("random", random);
        return "admin/login2";
    }
}
