package com.ssy.dao;

import com.ssy.domain.TimeDomain;

import java.util.List;


public interface TimeDao {
    int addTimeLine(TimeDomain timeDomain);

    int delTimeLine(Integer timeId);

    int updateTimeLine(TimeDomain timeDomain);

    List<TimeDomain> getTimeLine();

    TimeDomain getTimeLineByTimeId(Integer timeId);
}
