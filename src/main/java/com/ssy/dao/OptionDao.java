package com.ssy.dao;

import com.ssy.domain.OptionDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface OptionDao {
    /**
     * 删除网站配置
     */
    int delOptionByName(String name);

    /**
     * 更新网站配置
     */
    int updateOptionByName(OptionDomain options);

    /***
     * 根据名称获取网站配置
     */
    OptionDomain getOptionByName(String name);

    /**
     * 获取全部网站配置
     */
    List<OptionDomain> getOptions();
}
