package com.ssy.dao;

import com.ssy.domain.AttAchDomain;
import com.ssy.dto.AttAchDto;
import com.ssy.model.AttAchesVO;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

@Mapper
public interface AttAchDao {
    /**
     * 添加单个附件信息
     */
    int addAttAch(AttAchDomain attAchDomain);

    /**
     * 批量添加附件信息
     */
    int batchAddAttAch(List<AttAchDomain> list);

    /**
     * 根据主键编号删除附件信息
     */
    int delAttAch(int attAchId);

    /**
     * 更新附件信息
     */
    int updateAttAch(AttAchDomain attAchDomain);

    /**
     * 根据主键获取附件信息
     */
    AttAchDto getAttAchByAttAchId(int attAchId);

    /**
     * 获取所有的附件信息
     */
    List<AttAchDto> getAttAches();

    /**
     * 获取上传至七牛云的文章封面链接
     */
    List<AttAchesVO> getAttAchesByFileType(String fileType);

    /**
     * 查找附件的数量
     */
    Long getAttAchesCount();
}
