package com.ssy.dao;

import com.ssy.domain.UserConnectionDomain;
import com.ssy.dto.cond.EmailCond;
import com.ssy.dto.cond.UserConnectionCond;
import com.ssy.model.UserConnectionVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface UserConnectionDao {
    void addUserConnection(UserConnectionCond userConnection);

    void addEmailByProviderUserId(EmailCond emailCond);

    ArrayList<String> getProviderUserIdList();

    UserConnectionDomain getProviderByProviderUserId(String ProviderUserId);

    List<UserConnectionVO> getUserHeadOrderByTime();

    UserConnectionVO getThirdUserInfoRecently();

    void delUserConnectionByProviderId(String providerId);

    void setUserIdNull(String providerId);

}
