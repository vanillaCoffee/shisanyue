package com.ssy.dao;

import com.ssy.dto.UserDto;
import com.ssy.dto.cond.UserCond;
import com.ssy.config.security.MyUserDetails;
import com.ssy.model.UserVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserDao {
    /**
     * 更新用户信息
     */
    int updateUserInfoByUserCond(UserCond userCond);


    /**
     * 更新用户账户信息
     */
    int updateUserAccountInfo(MyUserDetails userDetails);

    /**
     * 根据用户名获取用户信息(SpringSecurity userDetails)
     */
    MyUserDetails getUserInfoByUsername(@Param("username") String username);

    /**
     * 根据用户名获取用户信息(userDto)
     */
    UserDto getUserInfoByUsernameAndSysUser(@Param("username") String username);


    /**
     * 根据用户名获取用户信息(userVo)
     */
    UserVO getUserVoByUsername(@Param("username") String username);
}
