package com.ssy.dao;

import com.ssy.domain.RoleMenuDomain;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.util.List;
@Mapper
public interface RoleMenuDao {
    /**
     * 添加角色权限关联
     */
    int addRoleMenuRelationShip(RoleMenuDomain roleMenuDomain);

    /**
     * 删除角色权限关联
     */
    int delRoleMenuRelationShip(RoleMenuDomain roleMenuDomain);

    /**
     * 更新角色权限关联
     */
    int updateRoleMenuRelationShip(RoleMenuDomain roleMenuDomain);

    /**
     * 获取角色权限列表
     */
    List<String> getMenuByRelationShipAndRoleList(@Param("roleList") List<String> roleList);
}
