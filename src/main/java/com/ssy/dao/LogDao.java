package com.ssy.dao;

import com.ssy.domain.LogDomain;
import com.ssy.dto.cond.LogCond;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface LogDao {
    /**
     * 添加日志
     */
    int addLog(LogCond logCond);

    /**
     * 删除日志
     */
    int delLogById(Integer id);

    /**
     * 获取日志
     */
    List<LogDomain> getLogs();
}
