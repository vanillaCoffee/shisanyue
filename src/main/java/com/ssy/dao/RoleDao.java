package com.ssy.dao;

import com.ssy.domain.RoleDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface RoleDao {
    /**
     * 添加角色
     */
    int addRole(String roleName);

    /**
     * 删除角色
     */
    int delRole(Integer roleId);

    /**
     * 更新角色
     */
    int updateRole(RoleDomain roleDomain);


    /**
     * 获取角色列表
     */
    List<RoleDomain> getRole();


    /**
     * 根据角色编号获取角色
     */
    RoleDomain getRoleByRoleId(Integer roleId);
}
