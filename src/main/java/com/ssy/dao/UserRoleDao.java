package com.ssy.dao;

import com.ssy.domain.UserRoleDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface UserRoleDao {
    /**
     * 添加用户角色关联
     */
    int addUserRoleRelationShip(UserRoleDomain userRoleDomain);

    /**
     * 删除用户角色关联
     */
    int delUserRoleRelationShip(UserRoleDomain userRoleDomain);

    /**
     * 更新用户角色关联
     */
    int updateUserRoleRelationShip(UserRoleDomain userRoleDomain);

    /**
     * 获取用户角色关联列表
     */
    List<String> getRoleByRelationShipAndUsername(String username);
}
