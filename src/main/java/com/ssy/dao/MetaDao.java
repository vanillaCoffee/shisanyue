package com.ssy.dao;

import com.ssy.domain.MetaDomain;
import com.ssy.dto.MetaDto;
import com.ssy.dto.cond.MetaCond;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface MetaDao {
    /**
     * 添加分类
     */
    int addMeta(MetaDomain metaDomain);

    /**
     * 删除分类
     */
    int delMetaByMetaId(Integer metaId);

    /**
     * 更新分类
     */
    int updateMeta(MetaDomain metaDomain);

    /**
     * 更新分类名
     */
    int updateMetaName(MetaCond metaCond);

    /**
     * 根据编号获取项目
     */
    MetaDomain getMetaByMetaId(Integer metaId);

    /**
     * 根据条件查询获取分类列表
     */
    List<MetaDomain> getMetasByCond(MetaCond metaCond);

    /**
     * 根据条件查询获取分类列表(分页)
     */
    List<MetaDomain> getMetasByCondWithPage(MetaCond metaCond);

    /**
     * 根据类型获取meta数量
     */
    Long getMetasCountByMetaType(String type);

    /**
     * 根据参数查询获取分类列表
     */
    List<MetaDto> getMetaList(Map<String, Object> paraMap);
}
