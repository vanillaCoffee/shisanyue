package com.ssy.dao;

import com.ssy.domain.ContentMetaDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface ContentMetaDao {
    /**
     * 添加文章与分类关联(作品没有分类)
     */
    int addContentMetaRelationShip(ContentMetaDomain contentMetaDomain);

    /**
     * 根据文章编号和meta编号删除内容与分类关联
     */
    int delContentMetaRelationShip(ContentMetaDomain contentMetaDomain);

    /**
     * 根据文章编号删除关联
     */
    int delContentMetaRelationShipByContentId(Integer contentId);

    /**
     * 根据meta编号删除关联
     */
    int delContentMetaRelationShipByMetaId(Integer metaId);

    /**
     * 更新文章与分类关联
     */
    int updateContentMetaRelationShip(ContentMetaDomain contentMetaDomain);

    /**
     * 根据文章主键获取关联
     */
    List<ContentMetaDomain> getContentMetaRelationShipByContentId(Integer contentId);

    /**
     * 根据meta编号获取关联
     */
    List<ContentMetaDomain> getContentMetaRelationShipByMetaId(Integer metaId);

    /**
     * 获取数量
     */
    Long getCountByContentIdAndMetaId(Integer contentId,Integer metaId);
}
