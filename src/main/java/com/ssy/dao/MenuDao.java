package com.ssy.dao;

import com.ssy.domain.MenuDomain;
import com.ssy.domain.MetaDomain;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface MenuDao {
    /**
     * 添加权限
     */
    int addMenu(MetaDomain metaDomain);

    /**
     * 删除权限
     */
    int delMenu(Integer metaId);

    /**
     * 更新权限
     */
    int updateMenu(MetaDomain metaDomain);

    /**
     * 获取权限列表
     */
    List<MenuDomain> getMenu();
}
