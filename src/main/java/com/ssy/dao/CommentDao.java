package com.ssy.dao;

import com.ssy.domain.CommentDomain;
import com.ssy.dto.CommentDto;
import com.ssy.dto.cond.CommentCond;
import com.ssy.model.MessageVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface CommentDao {
    /**
     * 添加评论
     */
    int addComment(CommentDomain commentDomain);

    /**
     * 删除评论
     */
    int delComment(Integer commentId);

    /**
     * 更新评论状态
     */
    int updateCommentStatus(Integer commentId, String status);

    /**
     * 根据主键查找单条评论
     */
    CommentDto getCommentsByCommentId(Integer commentId);

    /**
     * 根据文章编号查找单条评论
     */
    List<CommentDomain> getCommentsByContentId(Integer contentId);

    /**
     * 根据文章编号获取评论列表--只显示通过审核的评论-正常状态的
     */
    List<CommentDomain> getCommentsByContentIdAndApproved(Integer contentId);

    /**
     * 根据条件获取评论/留言列表
     */
    List<CommentDto> getCommentsByCond(CommentCond commentCond);

    /**
     * 获取最近留言
     */
    List<MessageVO> getRecentlyMessage();

    /**
     * 获取评论条数
     */
    Long getCommentsCount();

    /**
     * 获取累计评论数(通过文章号)
     */
    Long getCommentsCountByContentId(Integer contentId);

    /**
     * 通过父id获取父昵称
     */
    String getParentNickNameByParentId(Integer parentId);
}
