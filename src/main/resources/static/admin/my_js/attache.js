function delPost(aid) {
    layui.use(['layer', 'jquery', 'table'], function () {
        let $ = layui.jquery;
        layer.confirm('确定删除该封面吗?', function (index) {
            const load = layer.load(1, {shade: 0.3});
            $.ajax({
                    url: '/admin/attach',
                    type: 'delete',
                    data: {attacheId: aid},
                    success: function (data) {
                        layer.close(load);
                        if (data.code === 0) {
                            layer.msg("删除成功!", {icon: 1, time: 2000});
                        } else {
                            layer.msg(data.msg, {icon: 2, time: 2000});
                        }
                        setTimeout(function () {
                            location.reload();
                        }, 1500)
                    }
                }
            );
            layer.close(index);
        });
    })
}