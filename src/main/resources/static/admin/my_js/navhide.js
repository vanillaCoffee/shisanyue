var isShow = 1;

function iconHide() {
    if (isShow === 1)
        hide();
    else
        show();
    isShow *= -1;
}

function hide() {
    $('.layui-side span').hide();
    $('.layui-side').animate({width: '48px'});
    $('.layui-body').animate({left: '35px'});
    document.getElementById('hide').className = "layui-color layui-icon layui-icon-spread-left";
}

function show() {
    $('.layui-side span').show();
    $('.layui-side').animate({width: '200px'});
    $('.layui-body').animate({left: '200px'});
    document.getElementById('hide').className = "layui-color layui-icon layui-icon-shrink-right";
}

function ulHide() {
    if (isShow === -1)
        show();
    isShow = 1;
}