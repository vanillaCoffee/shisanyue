function login(){
    layui.use(['layer', 'jquery', 'form'], function () {
        const $ = layui.jquery;
        const form = layui.form;
        form.on('submit(login)', function () {
            const username = $('#username').val();
            const password = $('#password').val();
            const params = $("#login").serialize();
            if (username && password !== '') {
                $.ajax({
                    url: '/admin/login',
                    type: 'post',
                    async: false,
                    data: params,
                    success: function (result) {
                        if (result && result.code === 0) {
                            layer.msg(result.msg || '登录成功', {icon: 1, time: 2000});
                            setTimeout(function () {
                                location.href = "/admin/index";
                            }, 2000);
                        } else {
                            layer.msg(result.msg || '登录失败', {icon: 2, time: 2000});
                            setTimeout(function () {
                                location.href = "/toLogin";
                            }, 2000);
                        }
                    }
                });
                return false;
            }
        })
    });
}

