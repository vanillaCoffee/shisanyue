function subComment() {
    layui.use(['layer', 'jquery', 'form'], function () {
        const $ = layui.jquery;
        const form = layui.form;
        form.on('submit(formComment)', function () {
            const params = $("#comment").serializeArray();
            $.ajax({
                url: '/comment/add',
                type: 'post',
                data: params,
                success: function (result) {
                    if (result && result.code === 0) {
                        layer.msg(result.msg || '评论成功', {icon: 1, time: 1000});
                        setTimeout(function () {
                            location.reload();
                        },1000);
                        $.ajax({
                            url: '/sendmail',
                            type: 'post',
                            data: params
                        })
                    } else if (result && result.code === 1){
                        layer.msg(result.msg || '评论失败', {icon: 2, time: 1000});
                        setTimeout(function () {
                            location.reload();
                        },1000)
                    } else if (result && result.code === 2){
                        emailLayer();
                    }

                }
            });
            return false;
        });
    })
}


function reSubComment(index) {
    layui.use(['layer', 'jquery', 'form'], function () {
        const $ = layui.jquery;
        const form = layui.form;
        form.on('submit(reComment)', function () {
            console.log(index);
            const params = $("#reComment"+index).serializeArray();
            $.ajax({
                url: '/comment/add',
                type: 'post',
                data: params,
                success: function (result) {
                    if (result && result.code === 0) {
                        layer.msg(result.msg || '评论成功', {icon: 1, time: 1000});
                        setTimeout(function () {
                            location.reload();
                        },1000);
                        $.ajax({
                            url: '/sendmail',
                            type: 'post',
                            data: params
                        });
                    } else if (result && result.code === 1){
                        layer.msg(result.msg || '评论失败', {icon: 2, time: 1000});
                        setTimeout(function () {
                            location.reload();
                        },1000)
                    } else {
                        emailLayer();
                    }

                }
            });
            return false;
        });

    })
}


function emailLayer() {
    layui.use(['layer','jquery', 'form'],function () {
        const layer = layui.layer;
        const $ = layui.jquery;
        const form = layui.form;
        layer.open({
            title: "第三方用户邮箱填补",
            type:1,
            area: '500px',
            content:$("#emailfall").html(),
            success: function () {
                form.on("submit(emailfall)",function () {
                    const params = $("#Emailfall").serializeArray();
                    $.ajax({
                        url: '/email/add',
                        type: 'post',
                        data: params,
                        success:function (result) {
                            if (result && result.code === 0) {
                                layer.msg(result.msg || '填补邮箱成功', {icon: 1, time: 1000});
                            } else {
                                layer.msg(result.msg || '填补邮箱失败', {icon: 2, time: 1000});
                            }
                            setTimeout(function () {
                                location.reload();
                            },1000)
                        }

                    });
                    return false;
                })
            }
        })
    })
}

function htmlEscape(text){
    return text.replace(/[<>"&]/g, function(match, pos, originalText){
        switch(match){
            case "<": return "&lt;";
            case ">":return "&gt;";
            case "&":return "&amp;";
            case "\"":return "&quot;";
        }
    });
}
